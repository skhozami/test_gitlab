from validlt.computemetrics.compute import run_validlt
from validlt.computekpi.metrics_versionlt import compute_versionlt_metrics
from validlt.compareversions.compare_versionslt import compare_lt, load_analyses

import os
import datetime
import re
import pickle
import constants as cst

#step 1
caps = [dir for dir in os.listdir("./resources/") if re.match("capsule*",dir)]      
for cap in caps:
    pathGT = "./resources/"+cap+"/GT/SimObjs_GT.csv" 
    pathLT = "./resources/"+cap+"/LT_outputs/ObjsLT_"+cst.VERSION+".csv" 
    period = None  
    date = datetime.datetime.now() 
    out = "./resources/"+cap+"/" # Output file path
    run_validlt(pathGT, pathLT, out, period, cst.VERSION, date)

#step 2
compute_versionlt_metrics("./resources/", caps, cst.VERSION, b_report=True)

#step 3
dic_kpimetrics_by_version, dic_motmetrics_by_version = load_analyses("./resources/Analyses_versionLT/", [cst.VERSION])

print(dic_kpimetrics_by_version[cst.VERSION])

data = dic_kpimetrics_by_version[cst.VERSION].loc[:, ['MISS%', 'SWITCH%', 'FP%', 'detection%', 'SE', 'PPV']]
data['MISS%'] = 100-data['MISS%']
data['SWITCH%'] = 100-data['SWITCH%']
data['FP%'] = 100-data['FP%']

attributes = list(data)
values = data.iloc[-1].tolist()

kpi = dict(zip(attributes, values))

print(kpi)

#optional
compare_lt("./resources/Analyses_versionLT/", [cst.VERSION])

#step 4


