# -*- coding: utf-8 -*-
"""
@Filename : 

Description : this module computes comparison between LT versions 

Created on Wed Sep 11 12:19:45 2019

@author: sarah.le-bagousse
"""

from math import pi
import matplotlib.pyplot as plt
import os.path
import os
import pickle
from PyPDF2 import PdfFileMerger #, PdfFileReader, PdfFileWriter
from fpdf import FPDF


def compare_lt(path, versions_LT):
    """
    Description: compare results between deux versions of lidar tracker  

    Args:
        path : path to record data
        versions_LT : versions of lidar tracker to compare

    Returns:
    
    """
    
    path_name = os.path.join(path, "comparison")
    
    os.makedirs(path_name, exist_ok=True)
    
    # Load analyse by version
    dic_kpimetrics_by_version, dic_motmetrics_by_version = load_analyses(path, versions_LT)
        
    # Rador chart for version lt comparison
    path_radarfig = create_radarplt(path, dic_kpimetrics_by_version)
    
    # Report of comparison
    path_radarpdf = generate_comparison_report(path, path_radarfig, versions_LT)
    
    plt.close()
    
    # delete temporaries files
    delete_files([path_radarfig, path_radarpdf])

    
def load_analyses(path, versions_LT):
    """
    Description: load analyses by version  

    Args:
        path : path to record data
        versions_LT : versions of lidar tracker to compare

    Returns:
        dic_kpimetrics_by_version : dictionary of kpi metrics by LT version
        dic_motmetrics_by_version : dictionary of mot metrics by LT version
    
    """
    
    dic_kpimetrics_by_version = {}
    
    dic_motmetrics_by_version = {}
    
    # load analyse by version LT 
    for version in versions_LT:
        
        name_file = "Metrics_" + version
    
        path_file = os.path.join(path, name_file, name_file)
  
        with open(path_file, 'rb') as pickleIn:
            
           dic_comparison = pickle.load(pickleIn)
        
        o_kpi = dic_comparison["o_kpi_{}".format(version)]
        
        df_mot_metrics = dic_comparison["df_mot_metrics_{}".format(version)]
        
        df_kpi = o_kpi.tab_metrics
        
        dic_kpimetrics_by_version[version] = df_kpi
        
        dic_motmetrics_by_version[version] = df_mot_metrics
    
    return dic_kpimetrics_by_version, dic_motmetrics_by_version


def create_radarplt(path, dic_kpimetrics_by_version): 
    """
    Description: create radar chart for each version of LT  

    Args:
        dic_kpimetrics_by_version : dictionary of kpi metrics by LT version

    Returns:
        path_radarchart : path of the image 
        
    """
    
    # Create the chart
    ax = plt.subplot(111, polar=True)
    plt.title("Comparison of lidar tracker versions, improvements scale")
    
    # load analyse by version LT 
    for version in dic_kpimetrics_by_version:
                
        df_kpi = dic_kpimetrics_by_version[version]
#        df_mot_metrics = dic_comparison["df_mot_metrics_{}".format(version)]
                
        data = df_kpi.loc[:, ['MISS%', 'SWITCH%', 'FP%', 'detection%', 'SE', 'PPV']]
        data['MISS%'] = 100-data['MISS%']
        data['SWITCH%'] = 100-data['SWITCH%']
        data['FP%'] = 100-data['FP%']
        
        Attributes = list(data)
        AttNo = len(Attributes)
        
        #Find the values and angles for Messi - from the table at the top of the page
        values = data.iloc[-1].tolist()
        values += values [:1]
        
        angles = [n / float(AttNo) * 2 * pi for n in range(AttNo)]
        angles += angles [:1]
        
        plt.xticks(angles[:-1],Attributes)
        
        ax.plot(angles,values, label=version)
        ax.fill(angles, values, alpha=0.1)
               
    # Add legend
    #plt.legend(loc='upper right', bbox_to_anchor=(0.1, 0.1), title="LT versions")
    ax.legend(loc='upper right',  bbox_to_anchor=(0.1, 0.1),  title="LT versions")
    
    path_radarchart = os.path.join(path, "comparison", "radarchart.png")
    plt.tight_layout()
    plt.savefig(path_radarchart)

    return path_radarchart


def generate_comparison_report(path, path_radarchart, versions_LT):
    """
    Description: generates comparison report  

    Args:
        path :
        path_radarchart :
        versions_LT : list of versions to compare

    Returns:
      
        
    """
    
    name_file = "Metrics_comparison.pdf"
        
    path_name = os.path.join(path, "comparison")
    
#    os.makedirs(path_name, exist_ok=True)
    
    path_file = os.path.join(path_name, name_file)
    
    merger = PdfFileMerger()
    
    for version in versions_LT:

        path_pdf = os.path.join(path, "Metrics_" + version, "Metrics_" + version + '.pdf')           
           
        merger.append(path_pdf)
   
    # Add Radar chart
    pdf = FPDF(orientation='P', unit='mm', format='A4')
    pdf.add_page()
    
    pdf.set_font("Arial", size=16)
    pdf.cell(200, 10, txt='Metrics by lidar tracker version : radar chart', ln=1, align="C")
    
    pdf.image(path_radarchart, 0, 50)
    
    path_radarpdf = os.path.join(path_name, "RADAR.pdf")
    pdf.output(path_radarpdf)
    
    merger.append(path_radarpdf)
    
    with open(path_file, "wb") as f:
        merger.write(f)

    return path_radarpdf


def delete_files(paths):
    """
    Description: deletes files 

    Args:
        paths : paths of files to delete

    Returns:
        
    """
    
    for path in paths:
        
        os.remove(path)


if __name__ == '__main__':
    
    path = "\\\\acsysteme.lan\\dfs\\data\\Renault\\Fusion\\Validation_Lidar\\DataValidLT\\Analyses_versionLT"

    versions_LT = ["S29_2019", "S32_2019", "S36_2019"]

    compare_lt(path, versions_LT)