# -*- coding: utf-8 -*-
"""
@Filename : compute_metrics

Description :

Created on oct. 16, 2019 at 11:51:37

@author: thomas.bardot
"""
import sys
import getopt
import os.path
import validlt
from validlt.utils import get_string_date
from datetime import datetime
from validlt.computemetrics.compute import run_validlt


def setup(argv, date):
    """
    Setup the tool from a list of arguments

    Args:
        argv: list of arguments given by the user
        date: test date in date format

    Returns: the gt_csv file, lt_csv_file and the full path of the result .zip file, the lidar tracker version and the period between 2 timestamps
    """
    descr = "This tool computes metrics in order to compare ground truth objects and lidar tracked objects " \
            " and then generate reports."
    string_date = get_string_date(date)
    default_filename = string_date + "_result.zip"
    current_path = os.path.abspath(os.getcwd())
    output_file = os.path.join(current_path, default_filename)  # Default value
    period = None
    version_LT = "v_unknown"
    gt_csv = ""
    lt_csv = ""

    try:
        opts, args = getopt.getopt(argv, "ho:p:v:", ["help", "output=", "period=", "version="])
        for opt, arg in opts:
            if opt in ('-h', '--help'):
                usage()
                sys.exit(0)
            elif opt in ("-o", "--output"):
                output_file = arg
            elif opt in ("-p", "--period"):
                period = arg
            elif opt in ("-v", "--version"):
                version_LT = arg
            else:
                msg = f"Option {opt} is not recognized."
                print(msg)
                usage()
                sys.exit(2)
        if len(args) == 2:
            gt_csv = args[0]
            lt_csv = args[1]
        else:
            msg = "Wrong number of arguments."
            print(msg)
            usage()
            sys.exit(2)
    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit(2)

    try:
        assert os.path.isdir(os.path.dirname(output_file))
    except Exception as e:
        print(f"Error occurs with arguments,  the repository {os.path.dirname(output_file)} does not exist. \n{e}")
        usage()
        sys.exit(1)
    try:
        assert os.path.isfile(gt_csv)
    except Exception as e:
        print(f"Error occurs with arguments,  the file {gt_csv} does not exist. \n{e}")
        usage()
        sys.exit(1)
    try:
        assert os.path.isfile(lt_csv)
    except Exception as e:
        print(f"Error occurs with arguments,  the file {lt_csv} does not exist. \n{e}")
        usage()
        sys.exit(1)

    output_file = os.path.abspath(output_file)

    return gt_csv, lt_csv, output_file, period, version_LT


def usage():
    """
    Define the tool usage when used in command line

    """
    msg = f"""
    Compute metrics tool. Version of validlt framework : {validlt.__version__}
    This tool computes metrics in order to compare ground truth objects and lidar tracked objects.

    Usage:
      python -m validlt.compute_metrics  [-h | --help] [-o <output_file>| --output=<output_file>] [-p <period>| --period <period>] [-v <version_LT>| --version <version_LT>] <gt_file> <lt_file>

    Options:
      -h --help                   show help
      -o --output                 specify the path and name of the .zip output file
      -p --period                 specify the maximum time period to synchronise two frames in millisecond 
      -v --version                specify the version of the lidar tracker, default : v_unknown

    Arguments:
      <gt_file>           .csv file which contains the ground truth objects.
      <lt_file>           .csv file which contains the lidar tracker objects. 
    """
    print(msg)


if __name__ == '__main__':
    # 1- Parse arguments
    date = datetime.now()
    gt_csv, lt_csv, output_file, period, version_LT = setup(sys.argv[1:], date)

    # 2- Call main function
    run_validlt(gt_csv, lt_csv, output_file, period, version_LT, date)