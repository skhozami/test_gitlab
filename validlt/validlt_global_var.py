# -*- coding: utf-8 -*-
"""
validlt_global_var - contains the global variables used by the project

@author: Thomas Bardot
"""
from enum import Enum
import json
import os

# VALIDLT_PATH = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
# RESOURCE_PATH = os.path.join(VALIDLT_PATH, "resources")

# INPUT_CONST_FILEPATH = os.path.join(RESOURCE_PATH, "constants.json")
# INPUT_KPI_FILEPATH = os.path.join(RESOURCE_PATH, "kpi.json")


verbose = True
log_file = None

# json_const = None
# log_file_path = os.path.join(VALIDLT_PATH,"log.txt")

# with open(INPUT_CONST_FILEPATH) as json_file:
#     json_const = json.load(json_file)


# class EObjectType(Enum):
#     UNKNOWN = 1
#     CAR = 2
#     TRUCK = 3
#     PEDESTRIAN = 4
#     OTHER = 5
