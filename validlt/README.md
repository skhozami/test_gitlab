*******
ValidLT
*******

Presentation
============

ValidLT is a Python package dedicated to the validation of the Lidar Tracker software.

Developped by the company `AcsystÃ¨me`_ on behalf of `Groupe Renault`_, it is intended to measure
the performance of tracking algorithms and check the non-regressions of the new versions.

The validLT package contains 3 different tools :

* **compute metrics** which computes metrics for a given version of Lidar Tracker, on a given test scenario,
* **compute kpi** which aggregates metrics of a set of scenarios and computes Key performance indicators for a given version of Lidar Tracker,
* **compare version** which compares the performance - i.e. the kpi - previously computed of a given set of versions of Lidar Tracker.

.. _AcsystÃ¨me: http://www.acsysteme.com
.. _Groupe Renault: https://group.renault.com/groupe/


Installation
============

Prerequisites
-------------

The installation of ValidLT package requires:

* Python v3.6
* Venv: if not already installed, install virtualenv with ``pip install virtualenv``

Procedure
----------------------
Creation of the virtual environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Create a dedicated directory for the virtual environment. We call it ``v_env_validlt``.

Create the virtual environment in this directory with the following command: ::

    python -m venv v_env_validlt

Take care that the version of Python used to create the virtual environment is equal to 3.6.

Activate the virtual environment v_env_validlt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
If using Windows, run the following command: ::

    \v_env_validlt\Scripts\activate.bat

If using Linux, run the following command: ::

    source v_env_validlt/bin/activate

Verify that the version of Python installed in the virtual environment is equal or greater than 3.6: ::

    python -V

Installation of validLT tool with its dependencies
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The validLT package is given through the ``validlt-0.0.3.tar.gz`` archive file.

First, upgrade pip to the latest version with: ::

    pip install --upgrade pip

If using Windows, install beforehand the Shapely module with the following command: ::

    pip install Shapely-1.6.4.post1-cp35-cp35m-win_amd64.whl

The ``Shapely-1.6.4.post1-cp35-cp35m-win_amd64.whl`` file is given within the project archive.

Then, the installation of the validLT package and of its dependencies is done by running the following command either on Windows or Linux platform: ::

    pip install validlt-0.0.2.tar.gz

Tools usage
==================
Before using a tool, check that the virtual environment is activated

Compute metrics tool
---------------------

This tool is located in the Python package ``validlt.computemetrics``
The **compute metrics** tool can be launched with command line or can be integrated in another tool

Inputs
~~~~~~~~~~~~
The tool takes two ``.csv`` files in argument which defines a test.

One ``.csv`` files defines a sequence of ground truth objects while the other ``.csv`` file defines a sequence of Lidar Tracker objects.

A ``.csv`` file format is as follows: ::

    timestamp(ms);id;posx(m);posy(m);posz(m);length(m);width(m);heigth(m);yaw(rad);velx(m/s);vely(m/s);class
    0.00;1.00;63.50;-0.00;0.00;4.00;2.00;0.00;0.00;0.00;0.00;2.00
    0.00;2.00;63.50;-3.50;0.00;4.00;2.00;0.00;0.00;0.00;0.00;2.00
    20.00;1.00;63.58;-0.00;0.00;4.00;2.00;0.02;0.00;0.00;0.00;2.00

Outputs
~~~~~~~~~~~~
The tool generates a ``.zip`` archive which contains the detailed test report and the figures used by this report.
The tool also generates a ``pickle`` file which stores an object which contains the computed metrics

Command line
~~~~~~~~~~~~~
The module used to interface the tool and the user is ``compute_metrics.py`` located in the ``validlt`` package.

*Compute metric* tool takes in argument at least two ``.csv`` files, one which contains the ground truth objects, one which contains the Lidar Tracker objects.

The option ``--help`` will help you by showing the tool's usage. ::

    python -m validlt.compute_metrics --help

It will show the tool's usage: ::

    Compute metrics tool. Version of validlt framework : 0.2
    This tool computes metrics in order to compare ground truth objects and lidar tracked objects.

    Usage:
      python -m validlt.computemetrics.compute  [-h | --help] [-o <output_file>| --output=<output_file>] [-p <period>| --period <period>] [-v <version_LT>| --version <version_LT>] <gt_file> <lt_file>

    Options:
      -h --help                   show help
      -o --output                 specify the path and name of the .zip output file
      -p --period                 specify the maximum time period to synchronise two frames in millisecond
      -v --version                specify the version of the lidar tracker, default : v_unknown

    Arguments:
      <gt_file>           .csv file which contains the ground truth objects.
      <lt_file>           .csv file which contains the lidar tracker objects.


As example, the tool can be launched with the following command on the input files given in the ``demo`` repository: ::

    python -m validlt.compute_metrics -o demo/resources/capsule_sc_3L_ego19ms_1Car_1Truck_1Motorbike/result.zip -v S36_2019 demo/resources/capsule_sc_3L_ego19ms_1Car_1Truck_1Motorbike/GT/SimObjs_GT.csv demo/resources/capsule_sc_3L_ego19ms_1Car_1Truck_1Motorbike/LT_outputs/ObjsLT_S36_2019.csv

The output file will then be generated in the capsule directory.

Integration
~~~~~~~~~~~~
In order to integrate this tool in Python code, the following function has to be imported ::

    from validlt.computemetrics.compute import run_validlt
    import os
    import datetime

    # Initialize the function parameters
    out = os.getcwd() # Output file path
    pathGT = "SimObjs_GT.csv" # Path of the ground truth .csv input file
    pathLT = "ObjsLT_S36_2019.csv" # Path of the lidar objects .csv input file
    period = None # Maximum time period to synchronise two frames in millisecond
    version_lt = "S36_2019" # Version of the Lidar Tracker used to generate the LT objects input file
    date = datetime.datetime.now() # The date of the test has to be passed in parameter

    # Launch the tool
    run_validlt(pathGT, pathLT, out, period, version_lt, date)

Compute kpi tool
---------------------

This tool is located in the Python package ``validlt.computekpi``.

The **compute kpi** tool can be launched with command line or can be integrated in another tool

Inputs
~~~~~~~
This tool aggregates the metrics previously computed by **compute metrics** for a given version of the Lidar Tracker.

The previously computed metrics are stored in several Pickle files.

Outputs
~~~~~~~
This tool generates a pikle object where the KPI and aggregated metrics are stored. A pdf report is also generated.

Command line
~~~~~~~~~~~~~
The module used to interface the tool and the user is ``compute_kpi.py`` located in the ``validlt`` package.

**Compute kpi** tool takes in argument at least the version of the Lidar Tracker for which the KPI have to be computed.

It can take in argument the list of the capsule names which are repository with a specific hierarchy where the data used for a test scenario are stored.

The option ``--help`` will help you by showing the tool's usage: ::

    python -m validlt.compute_kpi --help

It will show the tool's usage: ::

    Compute the overall metrics and KPI for a given version of Lidar Tracker. It aggregates the metrics previously
    computed on all the scenarios contained in the capsules passed in argument.
    Version of validlt framework : 0.2

    Usage:
      python -m validlt.compute_kpi  [-h | --help] [-r | --report] [-p <path>| --path <path>]
                                                      [-l <capsule_list>| --list <capsule_list>] <version_lt>

    Options:
      -h --help                   show help
      -r --report                 generate report
      -p --path                   specify the path where the 'capsules' - i.e. where the result of lidar tracker validation - are stored.
                                  If not specified, the current repository is used by default.
      -l --list                   List of capsule name for which the metrics have to be aggregated.Has to be quoted and separated with a space.
                                  Ex.: '"'capsule_sc_3L_ego19ms_1Car_1Truck_1Motorbike capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbike S36_2019'.
                                  If not specified, all the directories located in path are taken in account.

    Arguments:
      <version_lt>                Version of the Lidar Tracker for which the kpi have to be computed
      <capsule_list>              List of capsule name for which the metrics have to be aggregated.Has to be quoted and separated with a space.
                                  Ex.: '"'capsule_sc_3L_ego19ms_1Car_1Truck_1Motorbike capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbike S36_2019'

Integration
~~~~~~~~~~~~
In order to integrate this tool in Python code, the following function has to be imported ::

    from validlt.computekpi.metrics_versionlt import compute_versionlt_metrics
    import os.path
    import os.getcwd

    # Initialize the function parameters
    path = os.path.abspath(os.getcwd()) # Path where the capsule are stored
    capsule_list = ['capsule_sc_3L_ego19ms_1Car_1Truck_1Motorbike',
                    'capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbike',
                    'capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbikebehind',
                    'capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbikebehind_Lbend',
                    'capsule_sc_3L_ego30ms_1Car_2Truck_2Motorbike',
                    'capsule_sc_3L_ego30ms_1Car_2Truck_2Motorbike_Rbend',
                    'capsule_sc_3L_ego30ms_2Car_1Truck',
                    'capsule_sc_3L_ego36ms_2Car_1Truck_1Motorbike']
    version_lt = "S36_2019" # Version of the Lidar Tracker used to test

    # Launch the tool
    compute_versionlt_metrics(path, capsule_list, version_lt, b_report=True)

As example, the tool can be launched with the following command on the input files given in the ``demo`` repository: ::

    python -m validlt.compute_kpi -r -p demo/resources/ -l "capsule_sc_3L_ego19ms_1Car_1Truck_1Motorbike capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbike" S36_2019

The output files will then be generated in the demo/Analyses_versionLT.

Compare versions
-------------------

This tool is located in the Python package ``validlt.compareversions``.

The **compare versions** tool can be launched with command line or can be integrated in another tool

Inputs
~~~~~~~
This tool compares the KPI previously computed by **compute kpi** for a whole set of version of the Lidar Tracker.

The previously computed KPI are stored in several Pickle files.

Outputs
~~~~~~~
This tool generates a pdf report.

Command line
~~~~~~~~~~~~~
The module used to interface the tool and the user is ``compare_versions.py`` located in the ``validlt`` package.

**Compare versions** tool takes in argument at least the whole set of versions of the Lidar Tracker for which the KPI have to be compared.

The option ``--help`` will help you by showing the tool's usage: ::

    python -m validlt.compare_versions --help

It will show the tool's usage: ::

    Compare the performance of different versions of Lidar Tracker. Version of validlt framework : 0.2
    This tool compares the metrics and KPI previously computed for each version of Lidar tracker passed in argument.

    Usage:
      python -m validlt.compare_versions  [-h | --help] [-p <path>| --path <path>] <version_list>

    Options:
      -h --help                   show help
      -p --path                   specify the path where the KPI of the different versions of lidar tracker are stored.
                                  If not specified,  all the current active directory is taken into account.

    Arguments:
      <versions_list>             List of Lidar tracker version to be compared. Has to be quoted and separated with a space.
                                  Ex.: '"'S29_2019 S32_2019 S36_2019'

Integration
~~~~~~~~~~~~
In order to integrate this tool in Python code, the following function has to be imported ::

    from validlt.compareversions.compare_versionslt import compare_lt
    import os.path
    import os.getcwd

    # Initialize the function parameters
    path = os.path.abspath(os.getcwd()) # Path where the KPI are stored
    version_lt = "S36_2019" # Version of the Lidar Tracker used to test

    # Launch the tool
    compare_lt(path, versions_LT)

Licence
=======

These developments are the exclusive property of Renault.

Contacts
========

* **Thierry Mimar** - *Project leader* - `Thierry Mimar <mailto://thierry.mimar@acsysteme.com>`_
* **Syntyche GbÃ¨hounou** - *Scientific referee* - `Syntyche GbÃ¨hounou <mailto://syntyche.gbehounou@acsysteme.com>`_
* **Sarah Le Bagousse** - *Scientific developer* - `Sarah Le Bagousse <mailto://sarah.le-bagousse@acsysteme.com>`_
* **Thomas Bardot** - *Software architect* - `Thomas Bardot <mailto://thomas.bardot@acsysteme.com>`_