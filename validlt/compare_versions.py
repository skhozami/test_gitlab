# -*- coding: utf-8 -*-
"""
@Filename : compare_versions

Description :

Created on oct. 16, 2019 at 11:27:37

@author: thomas.bardot
"""
import getopt
import sys
import validlt
from validlt.compareversions.compare_versionslt import compare_lt
import os.path


def setup(argv):
    """
    Setup the tool from a list of arguments

    Args:
        argv: list of arguments given by the user

    Returns: the path where the capsules are stored and the list of Lt versions to compare
    """
    path = os.path.abspath(os.getcwd())
    version_list = []

    try:
        opts, args = getopt.getopt(argv, "hp:v:", ["help", "path=", "versionlist="])
        for opt, arg in opts:
            if opt in ('-h', '--help'):
                usage()
                sys.exit(0)
            elif opt in ("-p", "--path"):
                path = arg
            elif opt in ("-v", "--versionlist"):
                for argument in arg.split(' '):
                    version_list.append(argument)
            else:
                msg = f"Option {opt} is not recognized."
                print(msg)
                usage()
                sys.exit(2)
        if len(args) == 1:
            for argument in args[0].split(' '):
                version_list.append(argument)
        else:
            msg = "Wrong number of arguments."
            print(msg)
            usage()
            sys.exit(2)
    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit(2)

    try:
        path = os.path.abspath(path)
        assert os.path.isdir(path)
    except Exception as e:
        print(f"Error occurs with arguments,  the repository {os.path.dirname(path)} does not exist. \n{e}")
        usage()
        sys.exit(1)

    return path, version_list


def usage():
    """
    Define the tool usage when used in command line

    """
    msg = f"""
    Compare the performance of different versions of Lidar Tracker. Version of validlt framework : {validlt.__version__}
    This tool compares the metrics and KPI previously computed for each version of Lidar tracker passed in argument.

    Usage:
      python -m validlt.compare_versions  [-h | --help] [-p <path>| --path <path>] <version_list>

    Options:
      -h --help                   show help
      -p --path                   specify the path where the KPI of the different versions of lidar tracker are stored.
                                  If not specified,  all the current active directory is taken into account.

    Arguments:
      <versions_list>             List of Lidar tracker version to be compared. Has to be quoted and separated with a space.
                                  Ex.: '"'S29_2019 S32_2019 S36_2019'
    """
    print(msg)


if __name__ == '__main__':
    # 1- Parse arguments
    path, versions_LT = setup(sys.argv[1:])

    # 2- Call main function
    compare_lt(path, versions_LT)