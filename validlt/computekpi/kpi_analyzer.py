# -*- coding: utf-8 -*-
"""
kip analyser

This function return True or False depend on the analysis of the success of 
the scenario kpi given

Created on Thu Jun  6 12:20:57 2019

@author: sarah.le-bagousse
"""

from validlt.computemetrics.objectscomparison.classification_constants import d_association_class
from pycm import ConfusionMatrix
import numpy as np
import pandas as pd
import pickle
import datetime 

class KpiAnalyzer:
       
    def __init__(self, df_mapping_kpi, output_dir, version_LT="unknown", 
                 str_date=None, thres_overlap=50, thres_overlapLT=75, 
                 ang_resol=0.25, thres_overlap_FP_GT=50, delta_ts=40):
        """
        Analysis of kpi. 
        Analyses by distance to ego and classe of objects

        Args:
            seq_metrics: a sequence object including mapping 
            df_mapping_kpi: a dataframe of mapping ofr kpi analysis
            version_LT : version of lidar tracker
            output_dir: directory to save the kpianalyzer object
            str_date: date in string format
            thres_overlap: threshold of global overlap (intersection over union)
            thres_overlapLT: threshold of LT overlap
        """
        
        if str_date is None:
               date = datetime.datetime.now()
               str_date = f"{date.year}_{date.month}_" \
                          f"{date.day}_{date.hour}" \
                          f"_{date.minute}_{date.second}"
           
        self.str_date = str_date
        
        # Version lidar tracker
        self.version_LT = version_LT
                        
        # Directory
        self.output_dir = output_dir
                
        # dataframe of mapping
        self.df_kpi = df_mapping_kpi.copy()
        
        # Class of GT objects
        self.classGT = self.df_kpi['classGT'].unique()
                
        # Deletion of nan
        self.classGT = [elts for elts in self.classGT if isinstance(elts, str)]
        
        # Alphabetic sort
        self.classGT.sort()
                
        # Limits of distances for kpi
        self.kpi_distances = [0, 50, 90, 120, 150] #float('Inf')
        
        # Angluar resolution of lidar in rad
        self.ang_resol = ang_resol*np.pi/180
        
        # Dataframe of kpi and metrics for comparison
        self.tab_columns = ['dist2ego', 'class_gt', 'nb_objs', 'MISS%', 'SWITCH%', 'FP%', 'detection', 'detection%', 'inclusion', 'SE', 'PPV']
        self.tab_columns_fp_series = ['dist2ego', 'nb frames mean', ' nb frames STD', 'nb frames min', 'nb frames max', 'duration mean (ms)',  'duration STD (ms)']
        self.tab_columns_miss_series = ['dist2ego', 'class_gt', 'nb frames mean', ' nb frames STD', 'nb frames min', 'nb frames max', 'duration mean (ms)',  'duration STD (ms)']
        self.tab_columns_fp_over_gt = ['dist2ego', 'nb mean', 'STD', 'nb min', 'nb max']
        self.tab_metrics = None
        self.tab_metrics_fp_series = None
        self.tab_metrics_miss_series = None
        self.tab_metrics_fp_over_gt = None

        # Threshold for global overlap (intersection over union), 50% by default
        self.thres_overlap = thres_overlap 
        
        # Threshold for LT overlap, 75% by default
        self.thres_overlapLT = thres_overlapLT 
        
        self.thres_overlap_FP_GT = thres_overlap_FP_GT
        self.delta_ts = delta_ts
        
        
    def compute_tab_metrics(self):
        """
        compute metrics for kpi, specifics distances and classes objects
        The metrics table is filled.
        """
        np.set_printoptions(precision=2)
        
        l_kpi_metrics = []
        l_kpi_metrics_fp_series = []
        l_kpi_metrics_miss_series = []
        l_kpi_metrics_fp_over_gt = []
        
        
        for idx_dist in range(len(self.kpi_distances)-1):
           
            # Distances interval
            d_inf = self.kpi_distances[idx_dist]
            d_sup = self.kpi_distances[idx_dist+1]
            
            # Extract data for the specified distances
            df_by_dist = self.df_kpi[(self.df_kpi['dist2ego'] >= d_inf) & (self.df_kpi['dist2ego'] < d_sup)]
            
            # Distances to ego
            dist2ego = str(d_inf) + '-' + str(d_sup)        
            
            if df_by_dist.empty:
                
                data_temp = [dist2ego, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan]                
                l_kpi_metrics.append(data_temp)
                
                l_kpi_metrics_fp_series.append([dist2ego, '-', '-','-', '-', '-','-'])
                l_kpi_metrics_fp_over_gt.append([dist2ego, '-', '-','-', '-'])
                l_kpi_metrics_miss_series.append([dist2ego, 'all', '-', '-','-', '-', '-','-'])
                for classGT in self.classGT:
                    l_kpi_metrics_miss_series.append([dist2ego, classGT, '-', '-','-', '-', '-','-'])
                
            else:
                
                # Update the FP over GT metrics
                l_metrics_fp_over_gt = compute_fp_over_gt_metrics(df_by_dist, self.thres_overlap_FP_GT)
                l_metrics_fp_over_gt.insert(0, dist2ego)
                l_kpi_metrics_fp_over_gt.append(l_metrics_fp_over_gt)
                
                # Update the FP serie metrics
                l_metrics_fp_series = compute_fp_series_metrics(df_by_dist, self.delta_ts)
                l_metrics_fp_series.insert(0, dist2ego)
                l_kpi_metrics_fp_series.append(l_metrics_fp_series)
                
                # Update the miss series metrics
                l_miss_series_metrics, d_miss_series_metrics = compute_miss_series_metrics(df_by_dist, self.delta_ts)
                l_miss_series_metrics.insert(0, dist2ego)
                l_miss_series_metrics.insert(1, 'all')
                l_kpi_metrics_miss_series.append(l_miss_series_metrics)
                # Update each class  metrics
                for classGT in self.classGT:
                    if classGT in d_miss_series_metrics:
                        l_miss_metrics_class = d_miss_series_metrics[classGT]
                        l_miss_metrics_class.insert(0, dist2ego)
                        l_miss_metrics_class.insert(1, classGT)
                        l_kpi_metrics_miss_series.append(l_miss_metrics_class)
                    else:
                        l_kpi_metrics_miss_series.append([dist2ego, classGT, '-', '-','-', '-', '-','-'])
                
                # Compute matrix confusion 
                self.compute_confusion_matrix(df_by_dist[(df_by_dist['Type'] == 'MATCH') | (df_by_dist['Type'] == 'SWITCH')])
               
                # Sensibility for each class
                classif_SE = classif_se(self.df_cm)
        
                # Positive predicted value for each class
                classif_PPV = classif_ppv(self.df_cm)
                
                # Number of false positives all classes
                nb_fp = compute_percentage( num_false_positives(df_by_dist), df_by_dist[(df_by_dist['Type'] != 'FP')].shape[0])
                
                for classGT in self.classGT:
                 
                    # Extract data by class
                    df_by_class = df_by_dist[df_by_dist['classGT'] == classGT]
                    
                    if df_by_class.empty:
                                                
                        data_temp = [dist2ego, classGT, np.nan, np.nan, np.nan, nb_fp, np.nan, np.nan, np.nan, np.nan, np.nan]
               
                        l_kpi_metrics.append(data_temp)
                        
                    else: 
                        
                        # Compute metrics by class and distances
                        nb_objs, nb_misses, nb_switches, df_tpdetection, nb_inclusions = self.compute_metrics_by_class(df_by_class)
                        
                        # Filled the metrics table
                        detection = df_tpdetection['tpdetection'].sum() 
                        detection_percentage = round(df_tpdetection['tpdetection'].sum() / nb_objs * 100, 2)
                        
                        # Check all misses 
                        if nb_objs == nb_misses or d_association_class[classGT] not in self.df_cm.index: # no classification
                            SE = np.nan
                            PPV = np.nan
                        else: 
                            SE = round(classif_SE[ list(self.df_cm.index).index(d_association_class[classGT]) ], 2)
                            PPV = round(classif_PPV[ list(self.df_cm.index).index(d_association_class[classGT]) ], 2)
                                                    
                        data_temp = [dist2ego, classGT, nb_objs, compute_percentage(nb_misses, nb_objs), compute_percentage(nb_switches, nb_objs), nb_fp, detection, detection_percentage, nb_inclusions, SE, PPV]
                
                        l_kpi_metrics.append(data_temp)
          
        # Dataframe of kpi metrics
        self.tab_metrics = pd.DataFrame(l_kpi_metrics, columns=self.tab_columns)
        self.tab_metrics_fp_series = pd.DataFrame(l_kpi_metrics_fp_series, columns=self.tab_columns_fp_series)
        self.tab_metrics_miss_series = pd.DataFrame(l_kpi_metrics_miss_series, columns=self.tab_columns_miss_series)
        self.tab_metrics_fp_over_gt = pd.DataFrame(l_kpi_metrics_fp_over_gt, columns=self.tab_columns_fp_over_gt)        

        # Global metrics
        self.tab_metrics = self.add_global_metrics()
       
                        
    def compute_metrics_by_class(self, df):
        """
        Compute metrics on a dataframe selected at a specific distance to ego
        and for one objects class
        
        Args:
            df : dataframe with mapping at specifics distance and class
        
        Returns: 
            nb_objs : number of gt objects to detect
            nb_misses : number of missed objects
            nb_switches : number of switches
            df_tpdetection : dataframe with an added columns about true positive detection
            nb_inclusions : number of inclusions
        """
        
        # Number of objects to detect
        nb_objs = num_objects(df)
        
        # Number of missed objects
        nb_misses = num_misses(df)
         
        # Number of switched objects
        nb_switches = num_switches(df)
        
        # Check the consistency of the detection        
        df_tpdetection = self.check_TPdetection(df)
        
        # Number of inclusions
        nb_inclusions = num_inclusions(df)
        
        return nb_objs, nb_misses, nb_switches, df_tpdetection, nb_inclusions
    
                    
    def add_global_metrics(self, df_metrics=None, df_kpi=None):
        """
        Addition of the global metrics in tab
        
        Args:
            tab_metrics: table of kpi metrics
            df_kpi: dataframe of mapping for kpi
        
        Returns: 
            tab_metrics: table with the sum of columns and global classfication 
        """
        
        if df_metrics is None:
            df_metrics = self.tab_metrics.copy()
         
        if df_kpi is None:
            df_kpi = self.df_kpi.copy()  
        
        # Sum of columns
        tab_metrics = df_metrics.append(df_metrics.sum(), ignore_index=True)
        
        # Index last line
        n_line = tab_metrics.shape[0]-1 
        idx_line = tab_metrics.index[n_line]
        
        # Assign the right values in last line on all scenarios
        tab_metrics.loc[idx_line, "dist2ego"] = "all"
        tab_metrics.loc[idx_line, "class_gt"] = "all"
        
        tab_metrics.loc[idx_line, "MISS%"] = round(np.nansum(df_metrics["nb_objs"].to_numpy() * df_metrics["MISS%"].to_numpy()) / tab_metrics.loc[idx_line, "nb_objs"], 2)
        tab_metrics.loc[idx_line, "SWITCH%"] = round(np.nansum(df_metrics["nb_objs"].to_numpy() * df_metrics["SWITCH%"].to_numpy()) / tab_metrics.loc[idx_line, "nb_objs"], 2)
        tab_metrics.loc[idx_line, 'detection%'] = round(np.nansum(df_metrics["nb_objs"].to_numpy() * df_metrics['detection%'].to_numpy()) / tab_metrics.loc[idx_line, "nb_objs"], 2)
       
#        tab_metrics.loc[idx_line,'detection%'] = round(tab_metrics.loc[idx_line, "detection"] * 100 / tab_metrics.loc[idx_line, "nb_objs"], 2)
        tab_metrics.loc[idx_line, "FP%"] = round(np.nansum(df_metrics["nb_objs"].to_numpy() * df_metrics['FP%'].to_numpy() / 3) / tab_metrics.loc[idx_line, "nb_objs"], 2)
#        tab_metrics.loc[idx_line, "FP"] = tab_metrics.loc[idx_line, "FP"]  / 3
        
        # global classification
        self.compute_confusion_matrix(df_kpi[(df_kpi['Type'] == 'MATCH') | (df_kpi['Type'] == 'SWITCH')])
                 
        tab_metrics.loc[idx_line, "SE"] = round(np.diag(self.df_cm.values)[:-1].sum() * 100/ (tab_metrics.loc[idx_line, "nb_objs"] - tab_metrics.loc[idx_line, "MISS%"] * tab_metrics.loc[idx_line, "nb_objs"] /100), 2)
        tab_metrics.loc[idx_line, "PPV"] = round(np.diag(self.df_cm.values)[:-1].sum() * 100 / self.df_cm.values[-1:,:-1].sum(), 2)
            
        return tab_metrics
        
    
    def check_TPdetection(self, df, thres_overlap=None, thres_overlapLT=None):
        """
        Check if the matched or switched object is correctly detected
        
        Args:
            df : dataframe of objects
            thres_overlap : threshold for overlap (intersection over union), percentage
            thres_overlapLT : threshold for LT overlap, percentage
        
        Returns:
            df_tpdetection : datafram of objects with a boolean columns 
                             1 : good detection
                             0 : bad detection
        """
        if thres_overlap is None: 
           thres_overlap = self.thres_overlap 
           
        if thres_overlapLT is None: 
           thres_overlapLT = self.thres_overlapLT 
           
        df_tpdetection = df.copy()
        np.diag(self.df_cm.values)
        df_tpdetection['tpdetection'] = [0] * df_tpdetection.shape[0]
        
        for idxline in range(df_tpdetection.shape[0]):
            
            # Check the good detection in the case of mapping            
            if df.Type.iloc[idxline] == "MATCH" or df.Type.iloc[idxline] == "SWITCH": 
            
                oBBGT = df.loc[df.index[idxline], 'BBGT']
            
                oBBLT = df.loc[df.index[idxline], 'BBLT']
                
                # areas of overlaps                          
                areaTruePos = oBBGT.intersection(oBBLT).area
                areaFalseNeg = oBBGT.get_contour().area - areaTruePos
                areaFalsePos = oBBLT.get_contour().area - areaTruePos          
            
                # Overlap GT
#                overlapGT = areaTruePos / oBBGT.get_contour().area
        
                # Overlap global rate (false positive area takes into account)
                overlap = areaTruePos / (areaTruePos +  areaFalseNeg +  areaFalsePos)
        
                # overlap LT
                if oBBLT.get_contour().area==0:
                    overlapLT = 1
                else:
                    overlapLT = areaTruePos / oBBLT.get_contour().area
            
                # Get objects corners
                oBBGT.get_corners()
                oBBLT.get_corners()
                corners_gt = np.array(oBBGT.mCorners)
                corners_lt = np.array(oBBLT.mCorners)
                
                # distance GT corners to lidar
                dist_gt = np.sqrt(corners_gt[:,0]**2 + corners_gt[:,1]**2)
                
                # arc between two lidar rays
                arc = 2 * dist_gt * self.ang_resol
                
                # Difference between corners GT and LT
                # TODO have corner in the same order !!!!!!!!!!
                if oBBLT.yaw > np.pi/2 or oBBLT.yaw < -np.pi/2 :
                    corners_lt = np.array([corners_lt[2],corners_lt[3],corners_lt[0],corners_lt[1]])
                    
                m_diff = abs(corners_gt - corners_lt)
                
                # Check the similarities between corners
                b_diff = m_diff < np.c_[arc,arc]
                
                v_diff = [True if b_diff[idx, 0] & b_diff[idx, 1] else False for idx in range(b_diff.shape[0])]
                
                # Number of similar corners
                num_corners_detect = v_diff.count(True)
                
                if num_corners_detect >= 2 and overlapLT > thres_overlapLT/100:
                    
                    # Check if object is front ego
                    if not np.all(corners_gt[:,1] > 0) and not np.all(corners_gt[:,1] < 0):
                        
                        df_tpdetection.loc[df_tpdetection.index[idxline], 'tpdetection'] = 1
                    
                    # Check global overlap 
                    elif overlap > thres_overlap/100:
                        
                        df_tpdetection.loc[df_tpdetection.index[idxline], 'tpdetection'] = 1
                    
        return df_tpdetection
    
         
    def compute_confusion_matrix(self, df):
        """
        Compute the confusion matrix on the attribution of class by lidar tracker
        
        Args:
            df : dataframe of mapping
        """
        
        np.set_printoptions(precision=2)
    
        if not df.empty:
            # Association of the corrects classes names between lidar tracker labels and simObject labels
            classes_gt = df['classGT'].tolist()
            
            for keys in list(set(classes_gt)):
                classes_gt = [x if x!= keys else d_association_class[keys] for x in classes_gt] 
            
            # Get classes names in the sequence
            self.classes_names = list(set(classes_gt + df['classLT'].tolist()))
            
            # Compute confusion matrix 
            if len(self.classes_names) == 1:
                
                # if one class
                class_gt = pd.Series(classes_gt, name='Actual')
                class_lt = pd.Series(df['classLT'].tolist(), name='Predicted')
                df_cm = pd.crosstab(class_gt, class_lt)   
            
            else:
                
                self.cm = ConfusionMatrix(classes_gt, df['classLT'].tolist())
             
                df_cm = pd.DataFrame(self.cm.matrix).T.fillna(0)
                
            # insert total column and line (the last ones)
            sum_col = []
            for c in df_cm.columns:
                sum_col.append( df_cm[c].sum() )
            sum_lin = []
  
            for item_line in df_cm.iterrows():
                sum_lin.append( item_line[1].sum() )
            df_cm['sum_lin'] = sum_lin
            sum_col.append(np.sum(sum_lin))
            df_cm.loc['sum_col'] = sum_col
            
            self.df_cm = df_cm
            
        else:
            
            self.df_cm = pd.DataFrame([])
        
    
    def save_tab_metrics(self):
        """
        record of the metrics table as a pickle objets
        """
        
        path =  self.output_dir + "\\" + 'kpianalyser_' + self.version_LT + "_" + self.str_date
                
        with open(path, 'wb') as pickleOut:
            pickle.dump(self, pickleOut) 
            
        print("Kpi analysis save in {}".format(path))


# %% Metrics by kpi
        
def num_objects(df):
    """ Number of objets """
    return df.shape[0]   

def num_matches(df):
    """Total number matches."""
    return df.Type.isin(['MATCH']).sum()

def num_switches(df):
    """Total number of track switches."""
    return df.Type.isin(['SWITCH']).sum()

def num_false_positives(df):
    """Total number of false positives (false-alarms)."""
    return df.Type.isin(['FP']).sum()

def num_misses(df):
    """Total number of misses."""
    return df.Type.isin(['MISS']).sum()

def num_inclusions(df):
    """ Number of inclusions """
    return df.inclusion.sum()

def classif_se(df_cm):
    """ 
    Percentage of sensibility about classification 
    
    Args:
        df_cm : dataframe of matrix confusion
    
    Returns:
        classif_SE: sensitivity values for each class
    """
           
    m_cm = df_cm.values.copy()
    
    # Avoid zero division
    m_cm[:,-1:][m_cm[:,-1:] == 0] = -1
    
    classif_SE = np.diag(m_cm) / m_cm[:,-1:].T * 100
    
    if classif_SE.size != 0:
        classif_SE = np.delete(classif_SE, -1)
        
    return classif_SE

def classif_ppv(df_cm):
    """ 
    Percentage of positives predictives values about classification
    
    Args:
        df_cm : dataframe of matrix confusion
    
    Returns:
        classif_PPV: postives predicted values for each class
    """

    m_cm = df_cm.values.copy()
    
    # Avoid zero division
    m_cm[-1:,:][m_cm[-1:,:] == 0] = -1
        
    classif_PPV = np.diag(m_cm) / m_cm[-1:,:] * 100
        
    if classif_PPV.size != 0:
        classif_PPV = np.delete(classif_PPV, -1)
        
    return classif_PPV


def compute_percentage(nobjs, ntotal):
    """
    Compute metrics on a dataframe selected at a specific distance to ego
    and for one objects class
    
    Args:
        nobjs : objects number under conditions
        ntotal : total objects number
    
    Returns: 
        percentage of the objects under condition
    """
    assert ntotal>0, "Total number must be strictly positive"  
    
    return round(nobjs * 100 / ntotal, 2)


# get the ranges of the constant value 
def find_temporal_series(t_timestamps, delta_ts):
    """
    Find temporal series in a timestamp array. The serie is defined as
    consecutive values. The delta_ts defines the maximum difference between
    two consecutive timestamps.
    
    Args:
        t_timestamps : an array of timestamps
        delta_ts : the time delta defining consecutive timestamps
    
    Returns: 
        l_ranges : the list ranges of the series (index of beginning and end)
        l_size_ranges: the list of the size of each series
    """
    
    l_ranges=[]
    l_size_ranges = []
    b_diff_values = np.diff(t_timestamps) <= delta_ts
    const_ind = np.where(b_diff_values)[0]
    diff_peaks_ind = np.where(np.bitwise_not(b_diff_values))[0]
    
    if len(const_ind) > 0:
        if len(diff_peaks_ind) > 0:
            start_range = const_ind[0]
            while True:                
                peak_after_range_ind= np.where(diff_peaks_ind > start_range)[0]            
                
                if len(peak_after_range_ind) > 0:
                    end_range = diff_peaks_ind[peak_after_range_ind[0]]                 
                else:
                    end_range = const_ind[-1]+1
                
                l_ranges.append([start_range, end_range])   
                l_size_ranges.append(end_range-start_range+1)             
                    
                if end_range < const_ind[-1]:                
                    start_range = const_ind[np.where(const_ind > end_range)[0][0]]
                else:
                    start_range = const_ind[-1]  
                    
                if start_range >= const_ind[-1]:
                    break
                
            return np.array(l_ranges), np.array(l_size_ranges)
        else:
            return np.array([[0, len(const_ind)]]), np.array([len(const_ind)+1])
    else:
        return np.array(l_ranges), np.array(l_size_ranges)  
        
    
def count_miss_series(df_mapping, delta_ts=40):
    """
    Count the consecutive temporal miss objects.
    
    Args:
        df_mapping : dataframe of mapping between the GT objects and the 
        tracked ones.
        delta_ts : the time delta defining consecutive timestamps
    
    Returns: 
        d_miss_series : an dictionnary containing the list miss series size for each class
        t_miss_serie_size: the list of the size of miss series for all objects
    """
    # get the object ID in the GT of the MISS objects 
    miss_id = np.unique(df_mapping['OId'][df_mapping['Type'] == 'MISS'])
    t_miss_serie_size = np.array([])
#    l_miss_size = []
    d_miss_series = dict()
        
    for oid in miss_id:
        # get the index where the GT objet with the ID oid was missed        
        miss_ind = df_mapping[np.bitwise_and(df_mapping['Type'] == 'MISS', \
                                             df_mapping['OId'] == int(oid))].index.to_numpy()
        
#        # Update the number of missing for each object
#        l_miss_size.append(len(miss_ind))
        
        _, serie_size = find_temporal_series(df_mapping['timestamp'].loc[miss_ind], delta_ts)
        
        t_miss_serie_size = np.append(t_miss_serie_size, serie_size)
        
        # get the object class
        object_class = df_mapping['classGT'].loc[miss_ind[0]]
        if object_class in d_miss_series:
            # Save the number of frame of the missing
            d_miss_series[object_class] = np.append(d_miss_series[object_class], serie_size)
        else:
            d_miss_series[object_class] = serie_size        
            
    return d_miss_series, t_miss_serie_size
            

def count_fp_series(df_mapping, delta_ts=40):
    """
    Count the consecutive temporal false positive objects.
    
    Args:
        df_mapping : dataframe of mapping between the GT objects and the 
        tracked ones.
        delta_ts : the time delta defining consecutive timestamps
    
    Returns: 
        t_fp_serie_size: the array of the size of FP series for all objects
    """
    
    # get the object ID in the LT of the False positive objects 
    fp_id = np.unique(df_mapping['HId'][df_mapping['Type'] == 'FP'])
#    l_fp_nb_frames = []
    t_fp_serie_size = np.array([])
    
    # Process object by object
    for oid in fp_id:
        # get the index where the GT objet with the ID oid was missed
        fp_ind = df_mapping[np.bitwise_and(df_mapping['Type'] == 'FP', df_mapping['HId'] == int(oid))].index.to_numpy()
        
#         # Update the number of false positive for each object
#        l_fp_nb_frames.append(len(fp_ind))
        
        _ , serie_size = find_temporal_series(df_mapping['timestamp'].loc[fp_ind], delta_ts)

        t_fp_serie_size = np.append(t_fp_serie_size, serie_size)
        
    return t_fp_serie_size


def count_fp_over_gt_metrics(df_mapping, thres_overlap_FP_GT=50):
    """
    Count the number of false positive boounding boxes that present a overlap
    with GT bounding box.
    
    Args:
        df_mapping : dataframe of mapping between the GT objects and the 
        tracked ones.
        thres_overlap_FP_GT : the overlap thresholding
    
    Returns: 
        t_fp_over_gt_nb: the array of the number of FP overlap with GT.
    """
    # Get the unique timestamp values
    t_timestamps = np.unique(df_mapping['timestamp'].to_numpy())
    t_fp_over_gt_nb = []
    
    # Search for the FP overlapping at each timestamp
    for ts in t_timestamps:        
        # Get all the FP bounding boxes        
        fp_bboxes = df_mapping['BBLT'][np.bitwise_and(df_mapping['Type'] == 'FP', df_mapping['timestamp'] == ts)].to_numpy()
        
        if len(fp_bboxes) > 0:
            # Get GT bounding boxes
            gt_bboxes = df_mapping['BBGT'][df_mapping['timestamp'] == ts].to_numpy()
            if len(gt_bboxes) > 0:
                for gt_bbox in gt_bboxes:
                    # Count the inclusion only for the bounding box is valid
                    try:
                        t_overlap_fp = np.array([(fp_bbox.intersection(gt_bbox).area/fp_bbox.get_contour().area)*100 for fp_bbox in fp_bboxes]) >= thres_overlap_FP_GT
                        t_fp_over_gt_nb.append(np.sum(t_overlap_fp))
                    except AttributeError:
                        #print("NaN GT bounding Box")
                        pass
                    except:
                       raise Exception("Exception in 'count_fp_over_gt_metrics'")   
                      
    return np.array(t_fp_over_gt_nb)  


def compute_miss_series_metrics(df_mapping, delta_ts=40):
    
    """
    Compute the metrics of the MISS series.
    
    Args:
        df_mapping : dataframe of mapping between the GT objects and the 
        tracked ones.
        delta_ts : the time delta defining consecutive timestamps
    
    Returns: 
        l_miss_series_metrics: the miss serie metrics : mean, std, min, max of 
        the number of the consecutive frames and the mean and std of the duration
        of the series.
        d_miss_series_metrics : the same metrics for each class of GT object
    """
    
    l_miss_series_metrics = []
    d_miss_series_metrics = dict()
    
    # Check if the df_mapping contains capsule IS
    if 'ID_capsule' in df_mapping.columns:
        t_id_capsule = np.unique(df_mapping['ID_capsule'])
        d_miss_series = dict()
        t_miss_serie_size = np.array([])
        
        # Count the serie for each capsule
        for id_capsule in t_id_capsule:
            d_miss_series_cap, t_miss_serie_size_cap = count_miss_series(df_mapping[df_mapping['ID_capsule']==int(id_capsule)], delta_ts)
            
            # Concatenate the capsule results
            t_miss_serie_size = np.append(t_miss_serie_size, t_miss_serie_size_cap)
            for key in d_miss_series_cap:
                if key in d_miss_series:
                    d_miss_series[key] = np.append(d_miss_series[key], d_miss_series_cap[key])
                else:
                    d_miss_series[key] = d_miss_series_cap[key]
            
    else:
        d_miss_series, t_miss_serie_size = count_miss_series(df_mapping, delta_ts)
        
    if len(t_miss_serie_size) > 0:
        l_miss_series_metrics = list(np.round([np.mean(t_miss_serie_size), \
                                               np.std(t_miss_serie_size),\
                                               np.min(t_miss_serie_size),\
                                               np.max(t_miss_serie_size),\
                                               np.mean(t_miss_serie_size*delta_ts),\
                                               np.std(t_miss_serie_size*delta_ts)], 3))
    else:
        l_miss_series_metrics = ['-']*6
        
    for key in d_miss_series:
        if len(d_miss_series[key]) > 0:
            d_miss_series_metrics[key] = list(np.round([np.mean(d_miss_series[key]),\
                                 np.std(d_miss_series[key]),\
                                 np.min(d_miss_series[key]),\
                                 np.max(d_miss_series[key]),\
                                 np.mean(d_miss_series[key]*delta_ts),\
                                 np.std(d_miss_series[key]*delta_ts)], 3))
        else:
            d_miss_series_metrics[key] = ['-']*6               
    
    return l_miss_series_metrics, d_miss_series_metrics


def compute_fp_series_metrics(df_mapping, delta_ts=40):
    
    """
    Compute the metrics of the False Positive series.
    
    Args:
        df_mapping : dataframe of mapping between the GT objects and the 
        tracked ones.
        delta_ts : the time delta defining consecutive timestamps
    
    Returns: 
        t_fp_series_metrics: the metrics of the FP boxes overlapping GT: mean, std, min, max 
    """
    
    # Check if the df_mapping contains capsule IS
    if 'ID_capsule' in df_mapping.columns:
        t_id_capsule = np.unique(df_mapping['ID_capsule'])
        t_fp_serie_size = np.array([])
        
        # Count the serie for each capsule
        for id_capsule in t_id_capsule:
            # Concatenate the capsule results
            t_fp_serie_size = np.append(t_fp_serie_size, count_fp_series(df_mapping[df_mapping['ID_capsule']==int(id_capsule)], delta_ts))
   
    else:
        t_fp_serie_size = count_fp_series(df_mapping, delta_ts)
        
    if len(t_fp_serie_size) > 0:
        t_fp_series_metrics = list(np.round([np.mean(t_fp_serie_size), \
                                               np.std(t_fp_serie_size),\
                                               np.min(t_fp_serie_size),\
                                               np.max(t_fp_serie_size),\
                                               np.mean(t_fp_serie_size*delta_ts),\
                                               np.std(t_fp_serie_size*delta_ts)], 3))
    else:
        t_fp_series_metrics = ['-']*6        
    
    return t_fp_series_metrics


def compute_fp_over_gt_metrics(df_mapping, thres_overlap_FP_GT=50):
    
    """
    Compute the metrics of the FP that are over GT.
    
    Args:
        df_mapping : dataframe of mapping between the GT objects and the 
        tracked ones.
        thres_overlap_FP_GT : the overlap thresholding
    
    Returns: 
        l_miss_series_metrics: the miss serie metrics : mean, std, min, max of 
        the number of the consecutive frames and the mean and std of the duration
        of the series.
        d_miss_series_metrics : the same metrics for each class of GT object
    """
    
    # Check if the df_mapping contains capsule ID
    t_fp_over_gt_nb = np.array([])
    if 'ID_capsule' in df_mapping.columns:
        t_id_capsule = np.unique(df_mapping['ID_capsule'])        
        
        # Count the serie for each capsule
        for id_capsule in t_id_capsule:            
            # Concatenate the capsule results
            t_fp_over_gt_nb = np.append(t_fp_over_gt_nb, count_fp_over_gt_metrics(df_mapping[df_mapping['ID_capsule']==int(id_capsule)], thres_overlap_FP_GT))
            
    else:                                                             
        t_fp_over_gt_nb = count_fp_over_gt_metrics(df_mapping, thres_overlap_FP_GT)
        
    # Only select the FP due to clustering error. When any FP overlaps GT
    # it means that it is a real False positive classification and  not a part
    # of GT.
    valid_fp_over_gt = t_fp_over_gt_nb[t_fp_over_gt_nb > 0]
    if len(valid_fp_over_gt) > 0:
        l_fp_over_gt_metrics = list(np.round([np.mean(valid_fp_over_gt), \
                                              np.std(valid_fp_over_gt),\
                                              np.min(valid_fp_over_gt),\
                                              np.max(valid_fp_over_gt)], 3))
    else:
        l_fp_over_gt_metrics = ['-']*4                  
    
    return l_fp_over_gt_metrics   
    
    
# %%
if __name__ == '__main__':
    
#    file1 = "\\\\acsysteme.lan\\dfs\\data\\Renault\\Fusion\\Validation_Lidar\\DataValidLT\\Capsules_SimObject\\capsule_sc_3L_ego19ms_1Car_1Truck_1Motorbike\\kpianalyser_2019_8_26_16_57_35"
    file1 = "\\\\acsysteme.lan\\dfs\\data\\Renault\\Fusion\\Validation_Lidar\\DataValidLT\\Capsules_SimObject\\capsule_sc_3L_ego30ms_1Car_2Truck_2Motorbike\\kpianalyser_2019_8_27_16_50_27"
    
    with open(file1, "rb") as pickleIn:
        
        oKpiAnalyser = pickle.load(pickleIn)
    
#    # Dataframe of kpi and metrics for comparison
#    oKpiAnalyser.tab_metrics = pd.DataFrame([], columns=oKpiAnalyser.tab_columns)
#    
#    # delete nan
#    oKpiAnalyser.classGT = [elts for elts in oKpiAnalyser.classGT if isinstance(elts, str)]
#    
#    oKpiAnalyser.compute_tab_metrics()
    
    a1 = oKpiAnalyser.tab_metrics
        
  
#  file2 = "\\\\acsysteme.lan\\dfs\\data\\Renault\\Fusion\\Validation_Lidar\\DataValidLT\\Capsules_SimObject\\capsule_sc_3L_ego19ms_1Car_1Truck_1Motorbike\\kpianalyser_2019_8_26_16_59_38"
    file2 = "\\\\acsysteme.lan\\dfs\\data\\Renault\\Fusion\\Validation_Lidar\\DataValidLT\\Capsules_SimObject\\capsule_sc_3L_ego30ms_1Car_2Truck_2Motorbike\kpianalyser_2019_8_27_17_4_43"
    
    with open(file2, "rb") as pickleIn:
        
        oKpiAnalyser = pickle.load(pickleIn)
    
#    # Dataframe of kpi and metrics for comparison
#    oKpiAnalyser.tab_metrics = pd.DataFrame([], columns=oKpiAnalyser.tab_columns)
#        
#    oKpiAnalyser.compute_tab_metrics()
        
    a2 = oKpiAnalyser.tab_metrics
        
      