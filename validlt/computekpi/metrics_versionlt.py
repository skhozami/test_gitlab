# -*- coding: utf-8 -*-
"""
@Filename : metrics_versionlt

Description : This module combines scenarios from the given list and 
its computes kpi metrics. Datafame of metrics is recorded azs pickle objet. 

Created on Thu Sep 10 09:06:49 2019

@author: sarah.le-bagousse
"""


from validlt.computekpi.kpi_analyzer import KpiAnalyzer
import pickle
import pandas as pd
from pandas.plotting import table
import matplotlib.pyplot as plt
import os.path

from fpdf import FPDF
import re


def compute_versionlt_metrics(path, l_capsules, version_LT, b_report=False, thres_overlap=50, thres_overlapLT=75, ang_resol=0.25):
    """
    Entry point of the version LT metrics computation

    Args:
        path : path of capsules
        l_files: list of scenarios to compare
        version_LT: list of the two version to compare 
        thres_overlap: threshold for acceptable overlap (by default 50%)
        thres_overlapLT: threshold for acceptable overlap LT (by default 75%)
        ang_resol: resolution horizonal of lidar in degre (0.25ÃÂ° for scala2)

    """
    
    # Combine some scenarios
    df_mapping_kpi, df_mot_metrics = combine_capsules(path, l_capsules, version_LT)
    
    # Compute KPI 
    o_kpi = compute_kpi(df_mapping_kpi, output_dir=path, version_LT=version_LT, thres_overlap=thres_overlap, thres_overlapLT=thres_overlapLT, ang_resol=ang_resol)
           
    # Save comparison as pickle object
    save_metrics_versionlt(path, o_kpi, df_mot_metrics, version_LT=version_LT)
    
    # Generation of report
    if b_report:    
        generate_report(path, l_capsules, version_LT, o_kpi, df_mot_metrics)
        
        
def combine_capsules(path, l_capsules, version_LT):
    """
    Description: combines capsules to compute metrics kpi on the scenarios list
    and mot metrics.

    Args:
        path : path of capsules
        l_capsules: list of scenarios to combine
        version_LT: the version of lidar tracker 

    Returns: 
        df_mapping_kpi: dataframe of mapping kpi with combine capsule for the precised LT version
        df_mot_metrics: dataframe of mot metrics with combine capsule for the precised LT version  
    """
    
    # Initialisation
    df_mapping_kpi = pd.DataFrame([])
    df_mot_metrics = pd.DataFrame([])
    
    # Combine each scenarios
    for id_capsule, capsule in enumerate(l_capsules):
        
        mapping_name = "mapping_kpi_" + version_LT
        
        file = os.path.join(path, capsule, mapping_name)
           
        # load capsule dictionnary for LT version n
        with open(file, "rb") as pickleIn:        
            dic_sequence = pickle.load(pickleIn)
        
        # Add an ID for capsule 
        df_mapping = dic_sequence["mapping_kpi"]
        df_mapping.insert(0, 'ID_capsule', id_capsule)
                    
        df_mapping_kpi = df_mapping_kpi.append(df_mapping, ignore_index=True)
        
        df_mot_metrics = df_mot_metrics.append(dic_sequence["mot_metrics"], ignore_index=True)
    
    df_mot_metrics = df_mot_metrics.round({'mota':2, 'motp':2})    
    
    return df_mapping_kpi, df_mot_metrics


def compute_kpi(df_mapping_kpi, output_dir="", version_LT="unknown", 
                thres_overlap=50, thres_overlapLT=75, ang_resol=0.25,
                thres_overlap_FP_GT=50, delta_ts=40):
    """
    Description: compute metrics for kpi analysis

    Args:
        df_mapping_kpi: dataframe of mapping of this version of lidar tracker
        output_dir: path of capsules
        version_LT: version of lidar tracker
        thres_overlap: threshold for acceptable overlap (by default 50%)
        thres_overlapLT: threshold for acceptable overlap LT (by default 75%)
        ang_resol: resolution horizonal of lidar in degre (0.25ÃÂ° for scala2)
        thres_overlap_FP_GT: the FP and GT overlapping thresholding
        delta_ts: the time delta defining consecutive timestamps

    Returns:
        kpi_analyser: object kpi analyzer of this lidar tracker version

    """
    
    # KPI analyse
    kpi_analyser = KpiAnalyzer(df_mapping_kpi, output_dir=output_dir, 
                               version_LT=version_LT, 
                               thres_overlap=thres_overlap, 
                               thres_overlapLT=thres_overlapLT, 
                               ang_resol=ang_resol, thres_overlap_FP_GT=50, 
                               delta_ts=40)

    kpi_analyser.compute_tab_metrics() 

    return kpi_analyser


#def compare_lt(o_kpi_v1, o_kpi_v2, df_mot_metrics_v1, df_mot_metrics_v2):
#    """
#    Description: compare results between deux versiosn of lidar tracker  
#
#    Args:
#        o_kpi_v1: object kpi analyzer of the previous version lidar tracker
#        o_kpi_v2: object kpi analyzer of the previous version lidar tracker
#        df_mot_metrics_v1: dataframe of mot metrics with combine capsule for previous LT version  
#        df_mot_metrics_v2: dataframe of mapping kpi with combine capsule for actual LT version
#
#    Returns:
#        tab_diff_metrics: kpi metrics difference between both versions of lidar tracker 
#        df_diff_mot_metrics: difference of global metrics by scenario between both versions of lidar tracker
#        
#    """
#    
#    # kpi metrics comparison
#    tab_diff_kpi = o_kpi_v1.tab_metrics.copy()
#    
#    tab_diff_kpi.iloc[:,3:] = o_kpi_v2.tab_metrics.iloc[:,3:] - o_kpi_v1.tab_metrics.iloc[:,3:]
#    
#    # global metrics comparison by scenario
#    df_diff_mot_metrics = df_mot_metrics_v1.copy()
#
#    df_diff_mot_metrics.iloc[:,2:] = df_mot_metrics_v2.iloc[:,2:] - df_mot_metrics_v1.iloc[:,2:]
#    
#    return tab_diff_kpi, df_diff_mot_metrics
    

def save_metrics_versionlt(path, o_kpi, df_mot_metrics, version_LT="unknown"):
    """
    Description: saves the results of the metrics for this LT version

    Args:
        path : path to record data
        o_kpi: object kpi analyzer of this LT version 
        df_mot_metrics: dataframe of mapping kpi with combine capsule for this LT version
        version_LT: version of lidar tracker
        
    """
    
    name_file = "Metrics_" + version_LT
    
    path_name = os.path.join(os.path.dirname(path), "Analyses_versionLT" , name_file)
    
    os.makedirs(path_name, exist_ok=True)
    
    path_file = os.path.join(path_name, name_file)
    
    # Dictionnary of data to save
    pickleData = {
        "o_kpi_{}".format(version_LT) : o_kpi,   
        "df_mot_metrics_{}".format(version_LT) : df_mot_metrics
        }
    
    with open(path_file, 'wb') as pickleOut:
        pickle.dump(pickleData, pickleOut) 
        
    print("Metrics for LT version {} save in {}".format(version_LT, path_name))


def load_metrics_versionlt(path, version_LT):
    """
    Description: loads the results of the comparison

    Args:
        path : path to record data
        version_LT: version of lidar tracker to load
               
    Returns:
        o_kpi: object kpi analyzer of this LT version 
        df_mot_metrics: dataframe of mapping kpi with combine capsule for this LT version
    
    """
    
    name_file = "Metrics_" + version_LT
    
    path_name = os.path.join(os.path.dirname(path), "Analyses_versionLT" , name_file)
    
    path_file = os.path.join(path_name, name_file)
    
    with open(path_file, 'rb') as pickleIn:
        dic_comparison = pickle.load(pickleIn)
        
    o_kpi = dic_comparison["o_kpi_{}".format(version_LT)]
    df_mot_metrics = dic_comparison["df_mot_metrics_{}".format(version_LT)]
    
    return o_kpi, df_mot_metrics
          

def save_as_png(path, o_kpi, df_mot_metrics, version_LT, ext_img=".png"):
    """
    Description: saves dataframes as images

    Args:
        path : path to record data
        o_kpi: object kpi analyzer of the version lidar tracker
        df_mot_metrics: dataframe of mot metrics with combine capsules for LT version  
        version_LT: version of lidar tracker
        ext_img: extension of images, PNG by default
        
    """
    
    dic_df_png = {
        "tab_kpi_{}".format(version_LT) : o_kpi.tab_metrics,
        "tab_kpi_FP_over_GT_{}".format(version_LT) : o_kpi.tab_metrics_fp_over_gt,
        "tab_kpi_FP_series_{}".format(version_LT) : o_kpi.tab_metrics_fp_series,
        "tab_kpi_MISS_series_{}".format(version_LT) : o_kpi.tab_metrics_miss_series,
        "df_mot_metrics_{}".format(version_LT): df_mot_metrics
        }
    
    for keys in dic_df_png:
        
        name_img = path + keys + ext_img
        df = dic_df_png[keys]
#        ax = plt.subplot(111, frame_on=False) # no visible frame
#        ax.xaxis.set_visible(False)  # hide the x axis
#        ax.yaxis.set_visible(False)  # hide the y axis
#    
#        table(ax, dic_df_png[keys])  # where df is your data frame
#    
#        plt.savefig(name_img)
         
        if "mota" in df.columns:
            df = df.round({'mota':2, 'motp':2})
        else:
            df = df.round({'detection%':2, 'SE':2, 'PPV':2})    
        
        fig, ax = plt.subplots() # set size frame figsize=(14, 10)
        ax.xaxis.set_visible(False)  # hide the x axis
        ax.yaxis.set_visible(False)  # hide the y axis
        ax.set_frame_on(False)  # no visible frame, uncomment if size is ok
        tabla = table(ax, df, loc='center', colWidths=[0.18]*len(df.columns))  # where df is your data frame

        tabla.auto_set_font_size(False) # Activate set fontsize manually
        tabla.set_fontsize(7) # if ++fontsize is necessary ++colWidths

        plt.tight_layout()
        plt.savefig(name_img, bbox_inches='tight', transparent=True) 
        plt.close(fig)
 
    
def add_dataframe_to_pdf(o_fpdf, df, section_title='', font='Arial', 
                         section_font_size=10, tab_header_font_size=9, 
                         text_font_size=8):
    """
    Ad a dataframe content to a ^df file. The different columns will have the 
    same size.
    
    Args:
        o_fpdf: the o
    
    """
    
    o_fpdf.set_font(font, size=section_font_size)
    o_fpdf.cell(0, 10, txt="", ln=1, align="L")
    o_fpdf.cell(0, 10, txt=section_title, ln=1, align="L")
    
    o_fpdf.set_font(font, size=tab_header_font_size)
       
    spacing = 1
    col_width = o_fpdf.w / (df.shape[1]+1)
    row_height = o_fpdf.font_size + 0.5
    
    for col in df.columns:
        o_fpdf.cell(col_width, row_height*spacing,
                     txt=col, border=1)
    o_fpdf.ln(row_height*spacing)
    
    o_fpdf.set_font(font, size=text_font_size)
    row_height = o_fpdf.font_size+0.5
    
    for idxline in range(df.shape[0]):
        for idxcol in range(df.shape[1]):
            o_fpdf.cell(col_width, row_height*spacing,
                     txt=str(df.iloc[idxline,idxcol]), border=1)
        o_fpdf.ln(row_height*spacing)
        
def generate_report(path, l_capsules, version_LT, o_kpi, df_mot_metrics, ext_img=".png"):
    """
    Description: generates report for a LT version

    Args:
        path : path to record data
        l_capsules: list of scenarios
        version_LT: version of lidar tracker 
        o_kpi: object kpi analyzer of the version lidar tracker
        df_mot_metrics: dataframe of mot metrics with combine capsules for LT version  
        ext_img: extension of images, PNG by default
        
    """
    
    name_file = "Metrics_" + version_LT
    
    path_name = os.path.join(os.path.dirname(path), "Analyses_versionLT" , name_file)
    
    path_file = os.path.join(path_name, name_file + '.pdf')
    
    pdf = FPDF(orientation='P', unit='mm', format='A4')
    pdf.add_page()
    
    # Title
    pdf.set_font("Arial", size=14)
    pdf.cell(200, 10, txt='Metrics of lidar tracker version {}'.format(version_LT), ln=1, align="C")
    pdf.cell(0, 10, txt="", ln=3, align="L")
      
    # Capsules list
    pdf.set_font("Arial", size=10)    
    pdf.cell(0, 10, txt="Capsules list :", ln=1, align="L")    
    for capsules in l_capsules:        
        pdf.cell(0, 6, txt=capsules, ln=1, align="L")
    
    # KPI table
    add_dataframe_to_pdf(pdf, o_kpi.tab_metrics, section_title="KPI table :", 
                         font='Arial', section_font_size=10, tab_header_font_size=8, 
                         text_font_size=7)
    
    add_dataframe_to_pdf(pdf, o_kpi.tab_metrics_fp_over_gt, 
                         section_title="KPI False Positive overlapping GT :", 
                         font='Arial', section_font_size=10, 
                         tab_header_font_size=8, text_font_size=7)
    
    add_dataframe_to_pdf(pdf, o_kpi.tab_metrics_fp_series, 
                         section_title="KPI consecutive False Positive", 
                         font='Arial', section_font_size=10, 
                         tab_header_font_size=8, text_font_size=7)
    
    add_dataframe_to_pdf(pdf, o_kpi.tab_metrics_miss_series, 
                         section_title="KPI consecutive MISS", 
                         font='Arial', section_font_size=10, 
                         tab_header_font_size=6, text_font_size=6)    
   
    # Mot metrics table by capsules
    pdf.cell(0, 10, txt="", ln=1, align="L")
    pdf.set_font("Arial", size=10)
    pdf.cell(0, 10, txt="Mot metrics table by capsules :", ln=1, align="L")
    
    pdf.set_font("Arial", size=6)
       
    spacing = 1
    col_width = pdf.w / (df_mot_metrics.shape[1])
    row_height = pdf.font_size + 0.5
    
    for col in df_mot_metrics.columns:
        if col == "mota" or col == "motp":
            col_width_temp = col_width*2/3
        else:
            col_width_temp = col_width
        
        pdf.cell(col_width_temp, row_height*spacing,
                     txt=col, border=1)
        
    pdf.ln(row_height*spacing)
        
    for idxline in range(df_mot_metrics.shape[0]):
        for idxcol in range(df_mot_metrics.shape[1]):
            
            if idxcol == df_mot_metrics.shape[1]-1 or idxcol == df_mot_metrics.shape[1]-2:
                col_width_temp = col_width*2/3
            else:
                col_width_temp = col_width
            
            pdf.cell(col_width_temp, row_height*spacing,
                     txt=str(df_mot_metrics.iloc[idxline,idxcol]), border=1)
        pdf.ln(row_height*spacing)
     
    # We can also set the file's metadata
    pdf.SetTitle = 'Metrics LT version {}'.format(version_LT)
    pdf.SetAuthor = 'Acsysteme'
    pdf.SetKeywords = 'Lidar_tracker metrics kpi mot_metrics'
    
    # Recording pdf
    pdf.output(path_file)


if __name__ == '__main__':
    
    path = "\\\\acsysteme.lan\\dfs\\data\\Renault\\Fusion\\Validation_Lidar\\DataValidLT\\Capsules_SimObject"

    l_capsules = ['capsule_sc_3L_ego19ms_1Car_1Truck_1Motorbike',
                  'capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbike',
                  'capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbikebehind',
                  'capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbikebehind_Lbend',
                  'capsule_sc_3L_ego30ms_1Car_2Truck_2Motorbike',
                  'capsule_sc_3L_ego30ms_1Car_2Truck_2Motorbike_Rbend',
                  'capsule_sc_3L_ego30ms_2Car_1Truck',
                  'capsule_sc_3L_ego36ms_2Car_1Truck_1Motorbike',
                  'capsule_sc_3L_ego36ms_2Car_1Truckfront_1Motorbike']

    version_LT = "S32_2019"

    compute_versionlt_metrics(path, l_capsules, version_LT, b_report=True)

    # argv = ['-r',
    #         '-p',
    #         "\\\\acsysteme.lan\\dfs\\data\\Renault\\Fusion\\Validation_Lidar\\DataValidLT\\Capsules_SimObject",
    #         '-l',
    #         '\"capsule_sc_3L_ego19ms_1Car_1Truck_1Motorbike\" ' +
    #         'capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbike ' +
    #         'capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbikebehind ' +
    #         'capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbikebehind_Lbend ' +
    #         'capsule_sc_3L_ego30ms_1Car_2Truck_2Motorbike ' +
    #         'capsule_sc_3L_ego30ms_1Car_2Truck_2Motorbike_Rbend ' +
    #         'capsule_sc_3L_ego30ms_2Car_1Truck ' +
    #         'capsule_sc_3L_ego36ms_2Car_1Truck_1Motorbike',
    #         'S32_2019']

    # path, gen_report, capsule_list, version_lt = setup(sys.argv[1:])
    # compute_versionlt_metrics(path, capsule_list, version_lt, b_report=gen_report)
       
#    o_kpi, df_mot_metrics = load_metrics_versionlt(path, version_LT=version_LT)
    
#    save_as_png(path, o_kpi, df_mot_metrics, version_LT=version_LT)
    