# -*- coding: utf-8 -*-
"""
@Filename : classification_constants

Description : contains the constants used to describe classification in SimObject and in lidar tracker

Created on Wed Jun  5 12:24:31 2019

@author: sarah.le-bagousse
"""

from enum import Enum
import pandas as pd

# SimObject classification
class ClassGT(Enum):

    UNKNOWN = 0         # unknown class
    UNKNOWN_SMALL = 1   # unknown class for small object
    UNKNOWN_BIG = 2     # unknown class for big object
    CAR = 3             # car
    BUS = 4             # bus
    TRUCK = 5           # truck
    TWO_WHEEL = 6       # two wheel object
    BIKE = 7            # bike
    BICYCLE = 8         # bicycle
    OVERDRIVABLE = 9    # overdrivable
    UNDERDRIVABLE = 10  # underdrivable
    TRAFFIC_LIGHT = 11  # traffic light
    TRAFFIC_SIGN = 12   # traffic sign
    CONSTRUCTION_SITE = 13 # construction site
    PEDESTRIAN = 14     # pedestrian
    GUARDRAIL = 15      # guardrail
    OTHER = 16          # other class
    

# Lidar tracker classification
class ClassLT(Enum):

    CL_UNDEF = 0
    CL_CAR = 1
    CL_TRUCK = 2
    CL_MOTORBIKE = 3
    CL_CONE = 4
    CL_ROADBOUNDARY = 5
    CL_UNRELIABLE = 6
    CL_INVALID = 7
    CL_SIZE = 8   
    
    
# old    
#    CL_UNDEF = 0
#    CL_CAR = 1
#    CL_TRUCK = 2
#    CL_ROADBOUNDARY = 3 
#    CL_INVALID = 4
#    CL_SIZE = 5
       
#    # old
#    CL_INVALID = 0
#    CL_UNDEF = 1
#    CL_CAR = 2
#    CL_TRUCK = 3
#    CL_ETC = 4
 
    
# Association dictionnary, simobject class to lidar tracker
d_association_class = {}
d_association_class['UNKNOWN'] = 'CL_UNDEF'
d_association_class['UNKNOWN_SMALL'] = 'CL_UNDEF'
d_association_class['UNKNOWN_BIG'] = 'CL_UNDEF'
d_association_class['CAR'] = 'CL_CAR'
d_association_class['BUS'] = 'CL_TRUCK'
d_association_class['TRUCK'] = 'CL_TRUCK'
d_association_class['TWO_WHEEL'] = 'CL_MOTORBIKE'
d_association_class['BIKE'] = 'CL_MOTORBIKE'
d_association_class['BICYCLE'] = 'CL_MOTORBIKE'
d_association_class['OVERDRIVABLE'] = 'CL_UNDEF'
d_association_class['UNDERDRIVABLE'] = 'CL_UNDEF'
d_association_class['TRAFFIC_LIGHT'] = 'CL_UNDEF'
d_association_class['TRAFFIC_SIGN'] = 'CL_UNDEF'
d_association_class['CONSTRUCTION_SITE'] = 'CL_UNDEF'
d_association_class['PEDESTRIAN'] = 'CL_UNDEF'
d_association_class['GUARDRAIL'] = 'CL_UNDEF'
d_association_class['OTHER'] = 'CL_UNDEF'


# Dataframe of names association of class objects 
dfAssociation_class = pd.DataFrame([], 
                                   columns = ['CAR', 'BUS', 'TRUCK', 'TWO_WHEEL', 'BIKE', 'PEDESTRIAN', 'OTHER', 'UNKNOWN'])

dfAssociation_class.loc['CL_CAR', :] = [1, 0, 0, 0, 0, 0, 0, 0]

dfAssociation_class.loc['CL_TRUCK', :] = [0, 1, 1, 0, 0, 0, 0, 0]

dfAssociation_class.loc['CL_UNDEF', :] = [0, 0, 0, 1, 1, 1, 1, 0]
    
dfAssociation_class.loc['CL_INVALID', :] = [0, 0, 0, 0, 0, 0, 0, 1] 




