# -*- coding: utf-8 -*-
"""
@Filename : frame_constants

Description : contains the constants used to describe dataframe columns

Created on mai 22, 2019 at 11:59:47

@author: thomas.bardot
"""


ID = 'id'
X = 'posx(m)'
Y = 'posy(m)'
YAW = 'yaw(rad)'
WIDTH = 'width(m)'
LENGTH = 'length(m)'
TIMESTAMP = 'timestamp(ms)'
IDFOV = 'idfov'