# -*- coding: utf-8 -*-
"""
tracked_object - model a tracked object

@author: Thomas Bardot
"""


class TrackedObject:
    def __init__(self, obj_id, bounding_box, obj_type, id_fov):
        """
        Args:
            obj_id(int): ID of the tracked object
            bounding_box(OrientedBoundingBox): bounding box related to the tracked object
            obj_type(EObjectType): type of the object (car, pedestrian, etc.)
            id_fov: id about position in the fov
                    0 : outside
                    1 : part is in
                    2 : totally inside
        """
        
        assert id_fov in [0,1,2], 'Position in fov not correct'  
        
        # Define all attributes
        self.id = obj_id
        self.bounding_box = bounding_box
        self.obj_type = obj_type
        self.id_fov = id_fov
