# -*- coding: utf-8 -*-
"""
ValidLT - Validation Lidar tracker module

Created on Fri Apr 19 16:15:22 2019

@author: sarah.le-bagousse
"""

import shapely.geometry as sg
import shapely.affinity as sa
import numpy as np
import matplotlib.pyplot as plt
import validlt.computemetrics.constants as cst


class OrientedBoundingBox:
    """
    Bounding box of an object
    """
    
    def __init__(self, cx, cy, size_x, size_y, yaw):
        """

        Args:
            cx: x position of bounding box center in meter
            cy: y position of bounding box center in meter
            size_x: size of the bounding box in meter along the x axis
            size_y: size of the bounding box in meter along the y axis
            yaw: orientation of bounding box in radian
        """

        self.cx = cx
        self.cy = cy
        self.size_x = size_x
        self.size_y = size_y
        self.yaw = yaw


    def get_contour(self, bRadian=True):
        """
        Oriented bounding box
        """
        size_x = self.size_x
        size_y = self.size_y
        c = sg.box(-size_x/2.0, -size_y/2.0, size_x/2.0, size_y/2.0)
        rc = sa.rotate(c, self.yaw, use_radians=bRadian)
        return sa.translate(rc, self.cx, self.cy)


    def intersection(self, other):
        """
        Get the intersection between 2 oriented bounding box
        """
        return self.get_contour().intersection(other.get_contour())
       
        
    def get_corners(self):
        """
        Get the fourth corners of the bounding box
            0: right front corner
            1: left front corner
            2: left rear corner
            3: right rear corner
        """
        oOrientBB = self.get_contour()
        self.mCorners =  list(oOrientBB.exterior.coords[:-1])
        

    def apply_fov(self, fov):
        """
        description : apply FOV and truncates objects if necessary
        
        args:
            fov : shapely object of the lidar FOV 
            
        returns:
            id_fov : indicator if object is in the fov
                        0 : outside
                        1 : part is in
                        2 : totally inside
        """
        
        # Initialisation in FOV
        id_fov = 2
        
        if fov is not None:
            id_fov = self.check_in_fov(fov)
            # half in FOV
            if id_fov == 1: 
                self.truncate_to_fov(ang_fov=cst.FOV_ANGDEG, pos_lidar=cst.FOV_POSLIDAR) 
        
        return id_fov
        
    
    def check_in_fov(self, fov):
        """
        description : compute the lidar fov
        
        args:
            fov : shapely object of the lidar FOV 
            
        returns:
            bInFOV : indicator if object is in the fov
                        0 : outside
                        1 : part is in
                        2 : totally inside
        """
        
        # Initialisation
        bInFOV = 0 # outside the fov
        
        # Check intersection between fov and obb
        InFOV = fov.intersection(self.get_contour())
        
        InFOV_area = np.round(InFOV.area, 1)
        
        # part is inside
        if InFOV_area == 0:
            bInFOV = 0 
         
        # part of object is inside FOV lear from ego
        elif InFOV_area < np.round(self.get_contour().area, 1) and self.cx < 100:
            bInFOV = 1
        
        # rear of object is inside FOV
        else:
            bInFOV = 2
    
        return bInFOV
    
    
    def truncate_to_fov(self, ang_fov=145, pos_lidar=[0,0], bVisu=False):
        """
        description : truncate bounding box which is half in FOV 
        
        args:
            ang_fov : angle of the lidar FOV in degrees
            pos_lidar : coordinate of lidar position
            bVisu : display the visualiation of fov with orinala and truncated bb
           
        returns:
            
        """
            
        # Point on limit line of FOV
        xFOV = 10 + pos_lidar[0]
        yFOV = np.tan(ang_fov/2 * np.pi/180) * xFOV + pos_lidar[1]
        
        # half line of FOV
        slope_fov = (yFOV-pos_lidar[1]) / (xFOV-pos_lidar[0]) 
        intercept_fov = yFOV - slope_fov * xFOV
        
        # Get corner
        self.get_corners()
        
        # Get front corners
        # TODO now it is not correct 
        # If the vehicle is in the reserve sense the front is the rear...
        if self.cy > pos_lidar[1]:
            corner_lat = self.mCorners[0]
            corner_diag = self.mCorners[1]
            corner_rearlat = self.mCorners[3]
        else:
            corner_lat = self.mCorners[1]
            corner_diag = self.mCorners[0]
            corner_rearlat = self.mCorners[2]
            slope_fov = -slope_fov
            intercept_fov = pos_lidar[1]+(pos_lidar[1]-intercept_fov)
            yFOV = pos_lidar[1]-(yFOV-pos_lidar[1])
               
        # line of lateral side ob object
        slope_obj = (corner_lat[1]-corner_rearlat[1]) / (corner_lat[0]-corner_rearlat[0]) 
        intercept_obj = corner_lat[1] - slope_obj * corner_lat[0]
            
        # Get intersection between object and FOV
        x_intersection = (intercept_obj - intercept_fov) / (slope_fov - slope_obj)
        y_intersection = slope_fov * x_intersection + intercept_fov
        
        # Get length
        length = np.sqrt( (corner_lat[0]-x_intersection)**2 + (corner_lat[1] - y_intersection)**2)
        
        # Get center coordinates
        x_c = (x_intersection + corner_diag[0]) / 2
        y_c = (y_intersection + corner_diag[1]) / 2
                
        # Visualisation
        if bVisu:
            
            oBB_trunc = OrientedBoundingBox(x_c, y_c, length, self.size_y, self.yaw)
            
            oBB_trunc.get_corners()
            # Initialise figure
            fig = plt.figure(figsize=(12, 6)) 
            ax = fig.add_subplot(111) 
            
            xold = [corner[0] for corner in self.mCorners]
            xold.append(xold[0])
            yold = [corner[1] for corner in self.mCorners]
            yold.append(yold[0])
             
            xnew = [corner[0] for corner in oBB_trunc.mCorners]
            xnew.append(xnew[0])
            ynew = [corner[1] for corner in oBB_trunc.mCorners]
            ynew.append(ynew[0])
            
            ax.plot([pos_lidar[0], xFOV], [pos_lidar[1], yFOV], '-r', label="FOV")
            ax.plot(xold, yold, '-g', label="BB original")
            ax.plot(xnew, ynew, '-b', label="BB trunc")
            
            ax.set_aspect('equal', 'box')
            ax.set_xlim((-5, 50))  
            ax.set_ylim((-10, 10)) 
            ax.set_title("Filtering FOV")
            ax.set_xlabel("length (m)")
            ax.set_ylabel("width (m)")
            ax.legend()
        
        # Modified values to fov
        self.cx = x_c
        self.cy = y_c
        self.size_x = length
        
        
#%%
if __name__ == '__main__':
    
    oBox = OrientedBoundingBox(0, 0, 1, 2, -np.pi)
    
    oBox.get_corners()
    
    print(oBox.mCorners)
