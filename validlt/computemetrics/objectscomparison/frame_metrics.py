# -*- coding: utf-8 -*-
"""
ValidLT - Validation Lidar tracker module

Created on Fri Apr 19 10:45:54 2019

@author: sarah.le-bagousse
"""

import numpy as np
import pandas as pd
from motmetrics.distances import norm2squared_matrix
import shapely.geometry as sg
import motmetrics as mm
from validlt.computemetrics.objectscomparison.oriented_bounding_box import OrientedBoundingBox
import matplotlib.pyplot as plt
from descartes import PolygonPatch
import validlt.computemetrics.objectscomparison.frame_constants as frame_cst
from validlt.computemetrics.objectscomparison.classification_constants import ClassGT, ClassLT


class FrameMetrics(object):
    """Information of one frame from a sequence."""

    def __init__(self, frame, maxD=float('inf')):
        """
        Initialisation frame object
        inputs:
             frame : objects related to one frame including
                     dfObjsGT / dfObjsLT : dataframe for ground truth objects
                                           columns : 'id' : id of objects
                                                     'posx(m)' : position of object center among x axis in meter
                                                     'posy(m)' : position of object center among y axis in meter
                                                     'yaw(rad)' : orientation of object in radian 
                                                     'width(m)' : width of object in meter
                                                     'length(m)' : length of object in meter
                                                     'timestamp(ms)' : timestamp in millisecond
            maxD : maximum distance accepted for mapping in meter
        """
        
        assert isinstance(frame.get_gt_df_objects(), pd.DataFrame) and isinstance(frame.get_lt_df_objects(), pd.DataFrame), "Objects must be dataframe type"
        
        try:            
            assert maxD > 0, "Maximal distance must be a positive value"   
        except TypeError:           
            raise TypeError("Maximal distance must be a number")
        
        # Object frame
        self.frame = frame
        
        # Dataframe of ground truth objects
        dfObjsGT = frame.get_gt_df_objects()
        
        # Dataframe of ground truth objects
        dfObjsLT = frame.get_lt_df_objects()
        
        # Delete obj outside FOV
        self.dfObjsGT = dfObjsGT[dfObjsGT['idfov']!=0]
        self.dfObjsLT = dfObjsLT[dfObjsLT['idfov']!=0]
        
        self.nObjsGT = self.dfObjsGT.shape[0]
        self.nObjsLT = self.dfObjsLT.shape[0]
        
        # Accumulator for one frame
        self.accFrame = mm.MOTAccumulator(auto_id=True)
        
        # Initialisation of counter
        self.nInclusion = 0 
        
        # Overlaps dataframe
        self.dfOverlaps = pd.DataFrame([], columns = ['timestamp', 'id_GT', 'id_LT', 'dist', 'overlapTP', 'overlap'])
        
        # Overlaps stats
        self.dfOverlaps_stats = pd.DataFrame([], columns = ['timestamp', 'mean', 'std', 'nb_objs'])
        
        # Inclusions dataframe
        self.dfInclusions = pd.DataFrame([], columns=['timestamp', 'id_GT', 'type', 'pos_x', 'pos_y'])
        
        # Classification dataframe
        self.dfClassification = pd.DataFrame([], columns=['timestamp', 'id_GT', 'class_GT', 'id_LT', 'class_LT'])
        
        # Orientation error dataframe
        self.dfOrientation = pd.DataFrame([], columns=['timestamp', 'id_GT', 'id_LT', 'yaw_err'])
        
        # Dataframe with all datas and metrics for classify ÃÂ  good detection
        self.df_detection = pd.DataFrame([])
        
        # Dataframe for computing kpi
        self.df_kpi = pd.DataFrame([], columns=['Type', 'OId', 'HId', 'D', 'timestamp', 'classGT', 'classLT', 'BBGT', 'BBLT', 'dist2ego', 'inclusion'])
        
        # Mapping
        self.compute_mapping(maxD=maxD)
        
        
    def compute_distances(self, maxD=float('inf'), method_point=0):
        """
        Computing distances between all GT and LT objects
        inputs:
            maxD : maximal distance to consider after considered as nan value
                   +inf default value
            method_point : enum to choose the point of comparison for mapping
                           0 : center of bounding box
                           1 : rear center of the bounding box
                           2 : rear center the nearest side from ego
        """
        try:            
            assert maxD > 0, "Maximal distance must be a positive value"   
        except TypeError:           
            raise TypeError("Maximal distance must be a number")
        
        # Center of bounding box
        if method_point==0:
            
            mObjsGTXY = self.dfObjsGT.loc[:,[frame_cst.X, frame_cst.Y]].to_numpy()
            mObjsLTXY = self.dfObjsLT.loc[:,[frame_cst.X, frame_cst.Y]].to_numpy()
        
        # Rear center of the bounding box 
        if method_point==1:
            
            # Compute the coordinates of the rear center 
            x_transGT = np.cos(self.dfObjsGT.loc[:,frame_cst.YAW].to_numpy()) * (self.dfObjsGT.loc[:,frame_cst.LENGTH].to_numpy()/2) 
            y_transGT = np.sin(self.dfObjsGT.loc[:,frame_cst.YAW].to_numpy()) * (self.dfObjsGT.loc[:,frame_cst.LENGTH].to_numpy()/2) 
            
            mObjsGTXY = self.dfObjsGT.loc[:,[frame_cst.X, frame_cst.Y]].to_numpy() - np.concatenate((np.array([np.abs(x_transGT)]).T, np.array([y_transGT]).T), axis=1) 
            
            x_transLT = np.cos(self.dfObjsLT.loc[:,frame_cst.YAW].to_numpy()) * (self.dfObjsLT.loc[:,frame_cst.LENGTH].to_numpy()/2) 
            y_transLT = np.sin(self.dfObjsLT.loc[:,frame_cst.YAW].to_numpy()) * (self.dfObjsLT.loc[:,frame_cst.LENGTH].to_numpy()/2) 
            
            mObjsLTXY = self.dfObjsLT.loc[:,[frame_cst.X, frame_cst.Y]].to_numpy() - np.concatenate((np.array([np.abs(x_transLT)]).T, np.array([y_transLT]).T), axis=1) 
        
        # Rear center the nearest side from ego
        if method_point==2:
            
            mObjsGTXY = np.empty((0,2))
            
            mObjsLTXY = np.empty((0,2)) 
            
            # Gt objects
            if not self.dfObjsGT.empty:
                
                for idGT in self.dfObjsGT[frame_cst.ID]:
          
                    # Bounding box of the GT obj
                    oBB_GT = self.frame.gt_objects[idGT].bounding_box
                
                    # Get corners the nearest from ego
                    oBB_GT.get_corners()
                    mCorners = np.array(oBB_GT.mCorners)
                    mCorners_sort = mCorners[mCorners[:,0].argsort()]
                                              
                    mObjsGTXY = np.concatenate((mObjsGTXY, np.array([[(mCorners_sort[0,0]+mCorners_sort[1,0])/2 , (mCorners_sort[0,1]+mCorners_sort[1,1])/2]])), axis=0)
            
            # Lt objects
            if not self.dfObjsLT.empty:
                
                for idLT in self.dfObjsLT[frame_cst.ID]:
                    
                    # Bounding box of the LT obj
                    oBB_LT = self.frame.lt_objects[idLT].bounding_box
                    
                    # Get corners the nearest from ego
                    oBB_LT.get_corners()
                    mCorners = np.array(oBB_LT.mCorners)
                    mCorners_sort = mCorners[mCorners[:,0].argsort()]
                                                
                    mObjsLTXY = np.concatenate((mObjsLTXY, np.array([[(mCorners_sort[0,0]+mCorners_sort[1,0])/2 , (mCorners_sort[0,1]+mCorners_sort[1,1])/2]])), axis=0)
            
            
        # Distances between all objects in a frame
        mDists = norm2squared_matrix(mObjsGTXY, mObjsLTXY, max_d2=maxD)
        self.mDists = np.sqrt(mDists)
        
        self.dfDists = pd.DataFrame(self.mDists, index=self.dfObjsGT[frame_cst.ID].tolist(), columns=self.dfObjsLT[frame_cst.ID].tolist())
        
        return np.sqrt(mDists)
    

    def compute_mapping(self, maxD=float('inf')):
        """
        Computes mapping between objects for this frame
        inputs:
            maxD : maximal distance to consider after considered as nan value
                  +inf default value
        """
        
        try:            
            assert maxD > 0, "Maximal distance must be a positive value"   
        except TypeError:           
            raise TypeError("Maximal distance must be a number")
         
        # Get distances
        self.compute_distances(maxD=maxD, method_point=2)
        
        # Frame accumulator update
        frameid = self.accFrame.update(
                  self.dfObjsGT[frame_cst.ID].tolist(), # Ground truth objects in this frame
                  self.dfObjsLT[frame_cst.ID].tolist(), # Detector hypotheses in this frame
                  self.mDists,  # Distances matrix between all objects and hypothesis 
                  )
                
        # Get mapping of this frame
        self.dfMapping = self.accFrame.mot_events.loc[frameid]
        
   
    def check_inclusions(self):
        """
        Detection of inclusions (two GT objects seen as one only object) 
        (a missed GT object included in a bounding box of a LT object)
        """
        
        # Initialisation of counter
        self.nInclusion = 0 
            
        # Research of missed GT objects  
        l_MissOId = self.dfMapping.loc[self.dfMapping['Type']=='MISS', 'OId'].tolist()
        
        # Check if missed objects centers is inside ÃÂ  LT bounding box objects
        for OId in l_MissOId:   
            
            # Parameters of missed object
            dfMissObj = self.dfObjsGT[self.dfObjsGT[frame_cst.ID] == OId]
            
            # Center of missed GT object
            point = sg.Point(dfMissObj.loc[dfMissObj.index[0], frame_cst.X], dfMissObj.loc[dfMissObj.index[0], frame_cst.Y])
            
            # Check if missed object center inside a LT object
            for idxObjLT in range(self.nObjsLT):
                
                if not self.dfMapping[(self.dfMapping['Type'] == 'MATCH') & (self.dfMapping['HId'] == self.dfObjsLT.id.iloc[idxObjLT])].empty:
                
                    dfLTObj = self.dfObjsLT.iloc[idxObjLT, :]
                                    
                    # Get oriented box of the LT object 
                    oOrientBox =  OrientedBoundingBox(dfLTObj.loc[frame_cst.X], dfLTObj.loc[frame_cst.Y],
                                                      dfLTObj.loc[frame_cst.LENGTH], dfLTObj.loc[frame_cst.WIDTH],
                                                      dfLTObj.loc[frame_cst.YAW])
                    
                    # Get coordinates of corners of bounding box
                    oOrientBox.get_corners()
                    
                    # Check the inclusion with a LT objects
                    polygon = sg.Polygon(oOrientBox.mCorners)
                
                    if polygon.contains(point):
                        self.nInclusion += 1
                        obj_type = self.frame.gt_objects[OId].obj_type
                        pos_x = self.frame.gt_objects[OId].bounding_box.cx
                        pos_y = self.frame.gt_objects[OId].bounding_box.cy
                        dfInclusion_obj  = pd.DataFrame(np.array([[self.frame.timestamp , OId, obj_type, pos_x, pos_y ]]),
                                                        columns=['timestamp', 'id_GT', 'type', 'pos_x', 'pos_y'])
                        
                        self.dfInclusions = self.dfInclusions.append(dfInclusion_obj, ignore_index=True)
                        continue
                
        return self.nInclusion


    def display_overlap(self, rectGT, rectLT, xLim=(-50, 50), yLim=(-10, 10)):
        """
        Display of overlap between two objets
        Input:
            rectGT : information of GT object rectangle, obj from OrientedBoundingBox class 
            rectLT : information of LT object rectangle, obj from OrientedBoundingBox class
            xLim : tuple, limits in meter for x axis
            yLim : tuple, limits in meter for y axis
        """
        
        fig = plt.figure(1, figsize=(10, 4))
        ax = fig.add_subplot(121)
        
        ax.axis('equal')
        
        ax.set_xlim(xLim)
        ax.set_ylim(yLim)
        
        ax.set_title('Overlap between object GT and tracked object')
        ax.set_xlabel('Rear to front (m)')
        ax.set_ylabel('Right to left (m)')
        
        p1 = PolygonPatch(rectGT.get_contour(), fc='#990000', ec='#990000', alpha=0.7)
        ax.add_patch(p1)
        
        p2 = PolygonPatch(rectLT.get_contour(), fc='#000099', ec='#000099', alpha=0.7)
        ax.add_patch(p2)
        
        p3 = PolygonPatch(rectGT.intersection(rectLT), fc='#009900', ec='#009900', alpha=1)
        ax.add_patch(p3)
        
        plt.legend([p1, p2, p3], ["Obj GT", "Obj LT", "Overlap"])
                                  
        plt.show()


    def compute_overlaps(self, bVisu=False):
        """
        getting overlap between 2 matched objects
        """
        
        # Research of matched objects  
        l_MatchOId = self.dfMapping.loc[self.dfMapping['Type']=='MATCH', 'OId'].tolist()
        l_MatchHId = self.dfMapping.loc[self.dfMapping['Type']=='MATCH', 'HId'].tolist()
        l_dist = self.dfMapping.loc[self.dfMapping['Type']=='MATCH', 'D'].tolist()
    
        # Compute overlap for each GT object   
        for idxObjsMatch in range( len(l_MatchOId) ):   
            
            # Bounding box of the GT obj
            oBB_GT = self.frame.gt_objects[l_MatchOId[idxObjsMatch]].bounding_box
            
            # Bounding box of the LT obj
            oBB_LT = self.frame.lt_objects[l_MatchHId[idxObjsMatch]].bounding_box
            
            # areas of overlaps                          
            areaTruePos = oBB_GT.intersection(oBB_LT).area
            areaFalseNeg = oBB_GT.get_contour().area - areaTruePos
            areaFalsePos = oBB_LT.get_contour().area - areaTruePos          
            
            # Overlap rate
            overlapTP = areaTruePos / oBB_GT.get_contour().area
        
            # Overlap rate (false positive area takes intot account)
            overlap = areaTruePos / (areaTruePos +  areaFalseNeg +  areaFalsePos)
          
            dfOverlap_obj = pd.DataFrame(np.array([[self.frame.timestamp , l_MatchOId[idxObjsMatch], l_MatchHId[idxObjsMatch], l_dist[idxObjsMatch], overlapTP, overlap]]),
                                         columns=['timestamp', 'id_GT', 'id_LT', 'dist', 'overlapTP', 'overlap'])
            
            self.dfOverlaps = self.dfOverlaps.append(dfOverlap_obj, ignore_index=True)
              
        # Compute overlap statistiques
        mean_overlap = self.dfOverlaps["overlapTP"].mean()
        std_overlap = self.dfOverlaps["overlapTP"].std()
        self.dfOverlaps_stats.loc[0,:] = np.array([[self.frame.timestamp, mean_overlap, std_overlap, len(l_MatchOId)]])
        
        # Visualisation
        if bVisu:           
            self.display_overlap(oBB_GT, oBB_LT)


    def compare_orientation(self):
        """
        compare orientation between 2 matched objects
        """
        
        # Research of matched objects  
        l_MatchOId = self.dfMapping.loc[self.dfMapping['Type']=='MATCH', 'OId'].tolist()
        l_MatchHId = self.dfMapping.loc[self.dfMapping['Type']=='MATCH', 'HId'].tolist()
        
        # Compute overlap for each GT object   
        for idxObjsMatch in range( len(l_MatchOId) ):  
            
            # Bounding box of the GT obj
            oBB_GT = self.frame.gt_objects[l_MatchOId[idxObjsMatch]].bounding_box
            
            # Bounding box of the LT obj
            oBB_LT = self.frame.lt_objects[l_MatchHId[idxObjsMatch]].bounding_box
            
            diff_yaw_GTvsLT = oBB_GT.yaw - oBB_LT.yaw
                  
            dfOrientation = pd.DataFrame(np.array([[self.frame.timestamp , l_MatchOId[idxObjsMatch], l_MatchHId[idxObjsMatch], diff_yaw_GTvsLT]]),
                                         columns=['timestamp', 'id_GT', 'id_LT', 'yaw_err'])
            
            self.dfOrientation = self.dfOrientation.append(dfOrientation, ignore_index=True)
            
                  
    def create_df_classification(self):
        """
        creation of dataframe with information about the classification between GT objects and their match Lt objects
        """  
         
        # Research of matched objects  
        l_MatchOId = self.dfMapping.loc[self.dfMapping['Type']=='MATCH', 'OId'].tolist()
        l_MatchHId = self.dfMapping.loc[self.dfMapping['Type']=='MATCH', 'HId'].tolist()     
         
        # Compute overlap for each GT object   
        for idxObjsMatch in range( len(l_MatchOId) ):    
            
            id_gtobj = l_MatchOId[idxObjsMatch]
            id_ltobj = l_MatchHId[idxObjsMatch]
            
            class_gtobj = self.frame.gt_objects[id_gtobj].obj_type
            class_ltobj = self.frame.lt_objects[id_ltobj].obj_type
            
            # Right names affection about class
            name_gt_class = ClassGT(class_gtobj).name
            name_lt_class = ClassLT(class_ltobj).name
            
#            if dfAssociation_class.loc[name_lt_class, name_gt_class]:
#                name_lt_class = name_gt_class
                       
            dfClassification_obj = pd.DataFrame(np.array([[self.frame.timestamp , id_gtobj, name_gt_class, id_ltobj, name_lt_class]]),
                                            columns=['timestamp', 'id_GT', 'class_GT', 'id_LT', 'class_LT'])
                       
            self.dfClassification  = self.dfClassification.append(dfClassification_obj, ignore_index=True) 
            
    
    def compute_metrics(self):
        """
        compute metrics for this frame
        """
        
        self.compute_overlaps()
        
        self.check_inclusion()
        
        self.compare_orientation()

        self.create_df_classification()
        
#        self.create_df_all()
    
    



        

                