# -*- coding: utf-8 -*-
"""
frame - model a frame

@author: Thomas Bardot
"""
import pandas as pd
import validlt.computemetrics.objectscomparison.frame_constants as frame_cst
from validlt.computemetrics.objectscomparison.tracked_object import TrackedObject

class Frame:
    def __init__(self, timestamp, lt_objects=[], gt_objects=[]):
        """
        Constructor
        Args:
            timestamp: timestamp of the frame in ms. We suppose that all objects of the frame have the same timestamp
            lt_objects(TrackedObject): list of lidar tracker objects
            gt_objects(TrackedObject): list of ground truth objects
        """
        # Dictionary of lt and gt objects {"id" : TrackedObject}
        self.lt_objects = {}
        self.gt_objects = {}

        # Dataframe attributes : they are filled automatically with construct_dataframe method
        self.__lt_df_objects = None
        self.__gt_df_objects = None
        self.init_lt_obj_dataframe()
        self.init_gt_obj_dataframe()

        # timestamp of the frame. All the gt and lt objects should have the same timestamp
        self.timestamp = timestamp

        # If some lt or gt objects are passed in argument, then add them to the objects dictionary
        self.add_lt_objects(lt_objects)
        self.add_gt_objects(gt_objects)

    def set_timestamp(self, timestamp):
        self.timestamp = timestamp

    def add_lt_object(self, lt_obj):
        """
        Append the lt_obj to the lt_objects dictionary. If the dictionary already contains lt_obj,
        then erase it.
        Args:
            lt_obj(TrackedObject): lt object
        """
        assert (lt_obj is not None) and (isinstance(lt_obj, TrackedObject)), "the object must be TrackedObject type"
        self.lt_objects[lt_obj.id] = lt_obj

    def add_gt_object(self, gt_obj):
        """
        Append the gt_obj to the gt_objects dictionary. If the dictionary already contains gt_obj,
        then erase it.
        Args:
            gt_obj(TrackedObject): gt object
        """
        assert (gt_obj is not None) and (isinstance(gt_obj, TrackedObject)), "the object must be TrackedObject type"
        self.gt_objects[gt_obj.id] = gt_obj

    def add_lt_objects(self, lt_objs):
        """
                Add a list of lt objects to the lt objects dictionary
                Args:
                    lt_objs: list of lt object of type TrackedObject
                """
        for obj in lt_objs:
            self.add_lt_object(obj)

    def add_gt_objects(self, gt_objs):
        """
        Add a list of gt objects to the gt objects dictionary
        Args:
            gt_objs: list of gt object of type TrackedObject
        """
        for obj in gt_objs:
            self.add_gt_object(obj)

    def set_lt_df_objects(self, lt_dataframe):
        """
        Set the lt objects dataframe attribute.
        Note : the method has to be used only for test or debug.
                Otherwise use set_lt_objects and construct_dataframes methods
        Args:
            lt_dataframe: dataframe of lt objects.
        """
        self.__lt_df_objects = lt_dataframe.copy()

    def set_gt_df_objects(self, gt_dataframe):
        """
        Set the gt objects dataframe attribute.
        Note : the method has to be used only for test or debug.
                Otherwise use set_gt_objects and construct_dataframes methods
        Args:
            gt_dataframe: dataframe of gt objects.
        """
        self.__gt_df_objects = gt_dataframe.copy()

    def get_lt_df_objects(self):
        """
        Returns: a copy of the lt objects dataframe
        """
        return self.__lt_df_objects.copy()

    def get_gt_df_objects(self):
        """
        Returns: a copy of the gt objects dataframe
        """
        return self.__gt_df_objects.copy()

    def construct_dataframes(self):
        """
        Construct the dataframe of lt and gt object from the dictionary of lt and gt objects
        """
        self.init_lt_obj_dataframe()
        self.init_gt_obj_dataframe()
        for id_obj in self.lt_objects.keys():
            new_df_obj = pd.DataFrame({frame_cst.ID: [id_obj],
                                       frame_cst.X: [self.lt_objects[id_obj].bounding_box.cx],
                                       frame_cst.Y: [self.lt_objects[id_obj].bounding_box.cy],
                                       frame_cst.YAW: [self.lt_objects[id_obj].bounding_box.yaw],
                                       frame_cst.WIDTH: [self.lt_objects[id_obj].bounding_box.size_y],
                                       frame_cst.LENGTH: [self.lt_objects[id_obj].bounding_box.size_x],
                                       frame_cst.TIMESTAMP: [self.timestamp],
                                       frame_cst.IDFOV: [self.lt_objects[id_obj].id_fov]
                                        })
            self.__lt_df_objects = self.__lt_df_objects.append(new_df_obj, ignore_index=True)

        for id_obj in self.gt_objects.keys():
            new_df_obj = pd.DataFrame({frame_cst.ID: [id_obj],
                                       frame_cst.X: [self.gt_objects[id_obj].bounding_box.cx],
                                       frame_cst.Y: [self.gt_objects[id_obj].bounding_box.cy],
                                       frame_cst.YAW: [self.gt_objects[id_obj].bounding_box.yaw],
                                       frame_cst.WIDTH: [self.gt_objects[id_obj].bounding_box.size_y],
                                       frame_cst.LENGTH: [self.gt_objects[id_obj].bounding_box.size_x],
                                       frame_cst.TIMESTAMP: [self.timestamp],
                                       frame_cst.IDFOV: [self.gt_objects[id_obj].id_fov]
                                        })
            self.__gt_df_objects = self.__gt_df_objects.append(new_df_obj, ignore_index=True)

    def init_lt_obj_dataframe(self):
        """
        Initialize lt objects dataframe
        """
        self.__lt_df_objects = pd.DataFrame({frame_cst.ID: [],
                                             frame_cst.X: [],
                                             frame_cst.Y: [],
                                             frame_cst.YAW: [],
                                             frame_cst.WIDTH: [],
                                             frame_cst.LENGTH: [],
                                             frame_cst.TIMESTAMP: [],
                                             frame_cst.IDFOV: []
                                             })

    def init_gt_obj_dataframe(self):
        """
        Initialize gt objects dataframe
        """
        self.__gt_df_objects = pd.DataFrame({frame_cst.ID: [],
                                             frame_cst.X: [],
                                             frame_cst.Y: [],
                                             frame_cst.YAW: [],
                                             frame_cst.WIDTH: [],
                                             frame_cst.LENGTH: [],
                                             frame_cst.TIMESTAMP: [],
                                             frame_cst.IDFOV: []
                                             })
    # def get_lt_id(self):
    #     return self.lt_object.keys()
    #
    # def get_gt_id(self):
    #     return self.gt_object.keys()
    #
    # def get_lt_x_y_coord(self):
    #     list_xy_coord= np.array()
    #     # for id_obj in self.lt_object.keys():
    #     #     np.append(list_xy_coord,
    #     #               [[self.lt_object[id_obj].bounding_box.cx,
    #     #                 self.lt_object[id_obj].bounding_box.cy]])
    #     list_xy_coord = self.lt_objects.loc[:, [X, Y]]
    #     return list_xy_coord

    @staticmethod
    def merge_frame(frame_1, frame_2, timestamp):
        """
        Merge the first frame with the second frame. The timestamp of the merged frame is the one passed in argument
        even if it is different from the timestamp of frame_1 or frame_2.
        Objects are compared with their ID: if two objects with the same id, only one of them is keep. Other object
        values are not compared. Then if two objects have the same Id but have different values (ex. diff bounding box)
        then only one of them is keep.
        Args:
            frame_1(Frame): first frame to merge
            frame_2(Frame): second frame to merge
            timestamp(Int): timestamp of the merged frame

        Returns: the resulting frame of the merge operation
        """
        assert isinstance(frame_1, Frame) and isinstance(frame_2, Frame)
        merge_frame = Frame(timestamp)
        merge_frame.add_lt_objects(frame_1.lt_objects.values())
        merge_frame.add_gt_objects(frame_1.gt_objects.values())
        merge_frame.add_lt_objects(frame_2.lt_objects.values())
        merge_frame.add_gt_objects(frame_2.gt_objects.values())

        return merge_frame


if __name__ == '__main__':
    from validlt.computemetrics.objectscomparison import TrackedObject
    from validlt.computemetrics.objectscomparison.oriented_bounding_box import OrientedBoundingBox

    # How to use frame
    timestamp = 0.40
    test_obj_1 = TrackedObject(1, OrientedBoundingBox(1, 2, 4, 5, 3), 1)
    test_obj_2 = TrackedObject(2, OrientedBoundingBox(2, 2, 4, 5, 3), 1)
    test_obj_3 = TrackedObject(1, OrientedBoundingBox(1, 2, 4, 5, 3), 1)
    test_obj_4 = TrackedObject(2, OrientedBoundingBox(2, 2, 4, 5, 3), 1)
    frame = Frame(timestamp)
    frame.add_lt_objects([test_obj_1, test_obj_2])
    frame.add_gt_objects([test_obj_3, test_obj_4])

