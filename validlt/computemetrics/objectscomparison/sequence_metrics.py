# -*- coding: utf-8 -*-
"""
ValidLT - Validation Lidar tracker module

Created on Fri Apr 19 10:44:26 2019

@author: sarah.le-bagousse
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
import os

import validlt.computemetrics.objectscomparison.plot_confusion_matrix as plt_cm

from pycm import ConfusionMatrix

import motmetrics as mm
from validlt.computemetrics.objectscomparison.frame_metrics import FrameMetrics
import validlt.computemetrics.objectscomparison.frame_constants as frame_cst

from validlt.computemetrics.objectscomparison.classification_constants import d_association_class, ClassGT, ClassLT


class SequenceMetrics(object):
    """Sequence to validate."""

    def __init__(self, seq, output_dir, maxD=1):
                
        # Sequence object
        self.seq = seq
        
        # Capsule directory
        self.output_dir = output_dir
        
        # Dictionary frame metrics
        self.dict_frames_metrics = {}                
         
        # Create an accumulator that will be updated during each frame
        self.accMetrics = mm.MOTAccumulator()#auto_id=True)
                
        # Dataframe of sequence metrics
        self.df_mot_metrics = pd.DataFrame([])
                      
        # Overlaps dataframe
        self.dfOverlaps = pd.DataFrame([], columns = ['timestamp', 'id_GT', 'id_LT', 'dist', 'overlapTP', 'overlap'])
        
        # Overlaps stats
        self.dfOverlaps_stats = pd.DataFrame([], columns = ['timestamp', 'mean', 'std', 'nb_objs'])
        
        # Inclusions dataframe 
        self.dfInclusions = pd.DataFrame([], columns = ['timestamp', 'id_GT', 'type', 'pos_x', 'pos_y'])
        
        # Number of inclusions for each frame        
        self.vInclusions = None 
        
        # Frame number of the first inclusion
        self.time_first_inclusion = None
        
        # First timestamp
        if len(self.seq.get_frames().keys()) > 0:
            self.first_timestamp = min(list(self.seq.get_frames().keys()))
        else:
            self.first_timestamp = 0
        
        # Lidar period
        self.period_lidar = list(self.seq.get_frames().keys())[1] - list(self.seq.get_frames().keys())[0]
            
        # Classification dataframe
        self.dfClassification = pd.DataFrame([], columns=['timestamp', 'id_GT', 'class_GT', 'id_LT', 'class_LT'])
        
        # Orientation error dataframe
        self.dfOrientation = pd.DataFrame([], columns=['timestamp', 'id_GT', 'id_LT', 'yaw_err'])
        
        # Confusion matrix on classification
        self.cm = None
        
        # Confusion matrix on classification dataframe
        self.df_cm = None
        
        # Classes names 
        self.classes_names = []
        
        self.df_kpi = pd.DataFrame([], columns=['Type', 'OId', 'HId', 'D', 'timestamp', 'classGT', 'classLT', 'BBGT', 'BBLT', 'dist2ego', 'inclusion'])
        
        # Maximum distance to consider mapping
        self.maxD = maxD
        
    def compute_metrics_by_frame(self):
        """
        Get mot metrics for all frames 
        """
        
        # Empty list
        vInclusions = []
        
        for timestamp in self.seq.get_frames().keys():
           
            # Creation frame object
            oFrame =  self.seq.get_frame(timestamp)
      
            # Creation of a frame metrics object
            oFrameMetrics = FrameMetrics(oFrame, maxD=self.maxD)
                                              
            # Update accumulator
#            self.accMetrics.update(oFrameMetrics.dfObjsGT[frame_cst.ID].tolist(), # Ground truth objects in this frame
#                                   oFrameMetrics.dfObjsLT[frame_cst.ID].tolist(), # Detector hypotheses in this frame
#                                   oFrameMetrics.compute_distances(),  # Distances matrix between all objects and hypothesis 
#                                   )
            
            self.accMetrics.update(oFrameMetrics.dfObjsGT[frame_cst.ID].tolist(), # Ground truth objects in this frame
                                   oFrameMetrics.dfObjsLT[frame_cst.ID].tolist(), # Detector hypotheses in this frame
                                   oFrameMetrics.mDists,  # Distances matrix between all objects and hypothesis 
                                   frameid = timestamp) # Frame Id 
                                                                   
            self.dict_frames_metrics[timestamp] = oFrameMetrics 
            
            # Compute overlaps between matched objects 
            oFrameMetrics.compute_overlaps()
                      
            self.dfOverlaps = self.dfOverlaps.append(oFrameMetrics.dfOverlaps, ignore_index=True)
        
            self.dfOverlaps_stats = self.dfOverlaps_stats.append(oFrameMetrics.dfOverlaps_stats, ignore_index=True)
            
            # Inclusion for each frame
            vInclusions.append(oFrameMetrics.check_inclusions())
            
            self.dfInclusions = self.dfInclusions.append(oFrameMetrics.dfInclusions, ignore_index=True)
            
            # Classification
            oFrameMetrics.create_df_classification()
            
            self.dfClassification = self.dfClassification.append(oFrameMetrics.dfClassification, ignore_index=True)
     
            # Orientation 
            oFrameMetrics.compare_orientation()
            
            self.dfOrientation = self.dfOrientation.append(oFrameMetrics.dfOrientation, ignore_index=True)
            
            # Dataframe for kpi // TODO
#            oFrameMetrics.construct_df_kpi()
#            
#            self.df_kpi = self.df_kpi.append(oFrameMetrics.df_kpi, ignore_index=True)   
            
            # To get switch values
            dfMapping = self.accMetrics.mot_events.loc[timestamp]
            
            df_kpi_by_frame = self.construct_df_kpi_by_frame(dfMapping, oFrameMetrics)
            
            self.df_kpi = self.df_kpi.append(df_kpi_by_frame, ignore_index=True)   
        
        # Timestamp in first columns
        cols = ['timestamp']  + [col for col in self.df_kpi if col != 'timestamp']
        self.df_kpi = self.df_kpi[cols]
        
        # Vector of inclusions by frame
        self.vInclusions = vInclusions

        
    def compute_metrics(self):
        """
        Get evaluation metrics for the entire sequence
        """
        print("compute metrics by frame \n")
        self.compute_metrics_by_frame()
        
        print("compute confusion matrix \n")
        self.compute_confusion_matrix()

#        print("Classification stats\n")
#        self.cm.print_stats()
#        print("\n ############### \n")
              
        mh = mm.metrics.create()
        
        self.df_mot_metrics = mh.compute(self.accMetrics, 
                             #metrics=mm.metrics.motchallenge_metrics,
                             metrics=['num_frames', 'num_unique_objects', 'mostly_tracked', 'mostly_lost', 'num_fragmentations', 'num_misses', 'num_false_positives', 'num_switches', 'mota', 'motp'],
                             name='accMetrics')

        strsummary = mm.io.render_summary(
                        self.df_mot_metrics, 
                        formatters=mh.formatters, 
                        #    namemap=mm.io.motchallenge_metric_names
                        )
        
        print(strsummary)
        
        # Inclusions
        print('Number of inclusions in sequence: {}'.format(sum(self.vInclusions)))
        
        if sum(self.vInclusions) != 0:
            self.time_first_inclusion = (np.nonzero(self.vInclusions)[0][0]+1)*self.period_lidar/1000
            print('First inclusion at {}s'.format(self.time_first_inclusion))
       
        
    # %% Confusion matrix
    
    def compute_confusion_matrix(self):
        """
        Compute the confusion matrix on the attribution of class by lidar tracker
        """
        
        np.set_printoptions(precision=2)
        
        # Association of the corrects classes names between lidar tracker labels and simObject labels
        classes_gt = self.dfClassification['class_GT'].tolist()
        
        for keys in list(set(classes_gt)):
            classes_gt = [x if x!= keys else d_association_class[keys] for x in classes_gt] 
        
        # Get classes names in the sequence
        self.classes_names = list(set(classes_gt + self.dfClassification['class_LT'].tolist()))
        
        # Compute confusion matrix 
        if len(self.classes_names) == 1:
            
            # if one class
            class_gt = pd.Series(classes_gt, name='Actual')
            class_lt = pd.Series(self.dfClassification['class_LT'].tolist(), name='Predicted')
            self.df_cm = pd.crosstab(class_gt, class_lt)   
        
        else:
            
            self.cm = ConfusionMatrix(classes_gt, self.dfClassification['class_LT'].tolist())
            self.df_cm = pd.DataFrame(self.cm.matrix).T.fillna(0)
        
        
    def display_confusion_matrix(self, path_name="Confusion matrix.png", annot=True, cmap="Oranges", fmt='.2f', fz=11,
                                 lw=0.5, cbar=False, figsize=[8,8], show_null_values=2, pred_val_axis='x'):
        """
        This function displays the confusion matrix between objects classes.
        input:
            path_name : path and name of the image to save
            annot : print text in each cell
            cmap : Oranges, Oranges_r, YlGnBu, Blues, RdBu, ...
            fz : fontsize
            lw : linewidth
            pred_val_axis : where to show the prediction values (x or y axis)
                           'col' or 'x': show predicted values in columns (x axis) instead lines
                           'lin' or 'y': show predicted values in lines   (y axis)
        """
        
        if self.df_cm is not None:
            plt_cm.pretty_plot_confusion_matrix(self.df_cm, path_name, annot=annot, cmap=cmap, fmt=fmt, fz=fz,
                                              lw=lw, cbar=cbar, figsize=figsize, show_null_values=show_null_values, pred_val_axis=pred_val_axis)
  
          
    # %% Dataframe for kpi
    
    def construct_df_kpi_by_frame(self, dfMapping, oFrameMetrics):
        """
        create a big dataframe contening all datas and metrics useful for kpi analyses 
        """
       
        df_kpi = dfMapping.copy()

        df_kpi['timestamp'] = [oFrameMetrics.frame.timestamp] * df_kpi.shape[0]
               
        df_kpi['classGT'] = [np.nan] * df_kpi.shape[0]
        
        df_kpi['classLT'] = [np.nan] * df_kpi.shape[0]    
        
        df_kpi['BBGT'] = [np.nan] * df_kpi.shape[0]
                
        df_kpi['BBLT'] = [np.nan] * df_kpi.shape[0]
        
        df_kpi['dist2ego'] = [np.nan] * df_kpi.shape[0]
        
        df_kpi['inclusion'] = [0] * df_kpi.shape[0]
              
        for idxline in range(df_kpi.shape[0]):
            
            id_gtobj = df_kpi['OId'].iloc[idxline]
            id_ltobj = df_kpi['HId'].iloc[idxline]
      
            if not np.isnan(id_gtobj): 
                
                # Bounding box of the GT obj
                oBBGT = oFrameMetrics.frame.gt_objects[ id_gtobj ].bounding_box
            
                df_kpi.loc[df_kpi.index[idxline], 'BBGT'] = oBBGT
                
                # Classes of objects     
                class_gtobj = oFrameMetrics.frame.gt_objects[id_gtobj].obj_type
                                     
                # Right names affection about class
                name_gt_class = ClassGT(class_gtobj).name
            
                df_kpi.loc[df_kpi.index[idxline], 'classGT'] = name_gt_class
                
                df_kpi.loc[df_kpi.index[idxline], 'dist2ego'] = np.sqrt(oBBGT.cx**2 + oBBGT.cy**2)
            
            else:
                
                # Bounding box of the LT obj  
                oBBLT = oFrameMetrics.frame.lt_objects[ id_ltobj ].bounding_box                 
                
                df_kpi.loc[df_kpi.index[idxline], 'BBLT'] =  oFrameMetrics.frame.lt_objects[ id_ltobj ].bounding_box
                
                df_kpi.loc[df_kpi.index[idxline], 'dist2ego'] = np.sqrt(oBBLT.cx**2 + oBBLT.cy**2)

              
            if not np.isnan(id_ltobj): 

                # Bounding box of the LT obj                   
                df_kpi.loc[df_kpi.index[idxline], 'BBLT'] =  oFrameMetrics.frame.lt_objects[ id_ltobj ].bounding_box
            
                # Classes of objects        
                class_ltobj = oFrameMetrics.frame.lt_objects[id_ltobj].obj_type
            
                # Right names affection about class
                name_lt_class = ClassLT(class_ltobj).name         

                df_kpi.loc[df_kpi.index[idxline], 'classLT'] = name_lt_class
            
            else:
                
                # Missed object, check inclusion
                if oFrameMetrics.dfInclusions.id_GT.isin([id_gtobj]).sum() > 0:
                    
                    df_kpi.loc[df_kpi.index[idxline], 'inclusion'] = 1
                
        
        return df_kpi
        
    
    # Record df_kpi as object with pickle
    def save_df_kpi(self, version_LT=""):
        """
        Record as object the dataframe df_kpi
        """
        
        if self.df_kpi.empty:
            raise ValueError("df_kpi is an empty dataframe, not saved")
            
        if self.df_mot_metrics.empty:
            raise ValueError("mot_metrics is an empty dataframe, not saved")
        
        path = os.path.join(self.output_dir, "mapping_kpi_" + version_LT)
        
        name_capsule = os.path.basename(self.output_dir)
        
        # Dictionnary of data to save
        pickleData = {
            "name_capsule":name_capsule,
            "version_LT":version_LT,
            "mapping_kpi":self.df_kpi,
            "mot_metrics":self.df_mot_metrics
            }
        
        with open(path, 'wb') as pickleOut:
            pickle.dump(pickleData, pickleOut) 
            
        print("Mapping dataframe save in {}".format(path))


    # %% Visualisation
                         
    def display_overlaps_objects_by_frame(self, list_id_obj=None, ax=None):
        """
        Visualisation of global overlaps (intersection over union) of objects list per frame
        """ 

        assert not self.dfOverlaps.empty, "Overlaps dataframe is empty"
          
        # Creation of figure
        if ax is None:
            fig = plt.figure(figsize=(10, 5))
            ax = fig.add_subplot(111)
        
        
        # With seaborn
        df_overlap = self.dfOverlaps.copy()
        df_overlap.overlap = df_overlap.overlap*100
        df_overlap.id_GT = df_overlap.id_GT.astype('int')
        df_overlap.timestamp = (df_overlap.timestamp.to_numpy() - self.first_timestamp)/1000
        
        sns.set_style("whitegrid", {'axes.grid' : False})
        
#        sns.barplot(x="timestamp", y="overlap", hue="id_GT", data=df_overlap)
        g = sns.pointplot(x="timestamp", y="overlap", hue="id_GT", data=df_overlap, scale=0.5, dodge=True)
        
#        # With matplotlib
#             
#        # Colors of objects
#        v_colors = plt.cm.get_cmap('hsv', len(list_id_obj)+1)
#        
#        # Markers of objects        
#        v_markers = ["+", "x", "o", "s", ">", "<", "v", "^"]
#        
#        for (idx, id_obj) in enumerate(list_id_obj):
#            
#            df_overlap_obj = self.dfOverlaps[self.dfOverlaps['id_GT'] == id_obj]
#        
#            plt.plot((df_overlap_obj['timestamp'].to_numpy() - self.first_timestamp)/1000, df_overlap_obj['overlapTP'].to_numpy()*100, 
#                     color=v_colors(idx), marker=v_markers[idx], fillstyle='none', linestyle='none')
#            
#        
        # Axis properties
        ax.set_title("Globals overlaps by frame between LT and GT matched objects")
        ax.set_xlabel('Time (s)')
        ax.set_ylabel('Overlap %')
        
        # Change xticks for readabilty
        g.set(xticks=np.arange(0,len(df_overlap["timestamp"].unique()),125))
        g.set(xticklabels=np.arange(0,df_overlap['timestamp'].to_numpy()[-1:]+1,5))
#        
#        ax.set_ylim((-0.2,103))
#
#        plt.legend([ 'obj_'+str(int(x)) for x in list_id_obj], loc=4)   
        
        
    def display_distances_objects_by_frame(self, list_id_obj=None, ax=None):
        """
        Visualisation of distances of objects list per frame
        """

        assert not self.dfOverlaps.empty, "Overlaps dataframe is empty"
        
        # All GT objects
        if list_id_obj is None:     
            list_id_obj = self.dfOverlaps['id_GT'].unique()
        
        # Creation of figure
        if ax is None:
            fig = plt.figure(figsize=(10, 5))
            ax = fig.add_subplot(111)
            
        # With seaborn
        df_dist = self.dfOverlaps.copy()
        df_dist.id_GT = df_dist.id_GT.astype('int')
        df_dist.timestamp = (df_dist.timestamp.to_numpy() - self.first_timestamp)/1000
        
        sns.set_style("whitegrid", {'axes.grid' : False})
        
#        sns.barplot(x="timestamp", y="dist", hue="id_GT", data=df_dist)
        g=sns.pointplot(x="timestamp", y="dist", hue="id_GT", data=df_dist, scale=0.5, dodge=True)
         
#        # With matplotlib
#        
#        # Colors of objects
#        v_colors = plt.cm.get_cmap('hsv', len(list_id_obj)+1)
#        
#        # Markers of objects        
#        v_markers = ["+", "x", "o", "s", ">", "<", "v", "^"]
#        
#        for (idx, id_obj) in enumerate(list_id_obj):
#            
#            df_overlap_obj = self.dfOverlaps[self.dfOverlaps['id_GT'] == id_obj]
#        
#            plt.plot((df_overlap_obj['timestamp'].to_numpy() - self.first_timestamp)/1000, df_overlap_obj['dist'].to_numpy(), 
#                     color=v_colors(idx),  marker=v_markers[idx], fillstyle='none', linestyle='none')
#            
#       # Axis properties 
        ax.set_title("Distances between GT objects and LT objects matches")
        ax.set_xlabel('Time (s)')
        ax.set_ylabel('Distances (m)')
        
        # Change xticks for readabilty
        g.set(xticks=np.arange(0,len(df_dist["timestamp"].unique()),125))
        g.set(xticklabels=np.arange(0,df_dist['timestamp'].to_numpy()[-1:]+1,5))
        
#        ax.set_ylim((-0.1, self.dfOverlaps['dist'].max()+0.1+10*self.dfOverlaps['dist'].max()/100))
#
#        plt.legend([ 'obj_'+str(int(x)) for x in list_id_obj])  
        
        
    def display_orientionserr_objects_by_frame(self, list_id_obj=None, ax=None):
        """
        Visualisation of orientation errors of objects list per frame
        """

        assert not self.dfOrientation.empty, "Orientation dataframe is empty"
        
        # All GT objects
        if list_id_obj is None:     
            list_id_obj = self.dfOrientation['id_GT'].unique()
        
        # Creation of figure
        if ax is None:
            fig = plt.figure(figsize=(10, 5))
            ax = fig.add_subplot(111)
            
        # With seaborn
        df_orientation = self.dfOrientation.copy()
        df_orientation.id_GT = df_orientation.id_GT.astype('int')
        df_orientation.timestamp = (df_orientation.timestamp.to_numpy() - self.first_timestamp)/1000
        
        sns.set_style("whitegrid", {'axes.grid' : False})
        
#        sns.barplot(x="timestamp", y="dist", hue="id_GT", data=df_dist)
        g=sns.pointplot(x="timestamp", y="yaw_err", hue="id_GT", data=df_orientation, scale=0.5, dodge=True)
         
#        # With matplotlib
#        
#        # Colors of objects
#        v_colors = plt.cm.get_cmap('hsv', len(list_id_obj)+1)
#        
#        # Markers of objects        
#        v_markers = ["+", "x", "o", "s", ">", "<", "v", "^"]
#        
#        for (idx, id_obj) in enumerate(list_id_obj):
#            
#            df_overlap_obj = self.dfOverlaps[self.dfOverlaps['id_GT'] == id_obj]
#        
#            plt.plot((df_overlap_obj['timestamp'].to_numpy() - self.first_timestamp)/1000, df_overlap_obj['dist'].to_numpy(), 
#                     color=v_colors(idx),  marker=v_markers[idx], fillstyle='none', linestyle='none')
#            
#       # Axis properties 
        ax.set_title("Orientation errors between GT objects and LT objects matches")
        ax.set_xlabel('Time (s)')
        ax.set_ylabel('Error (rad)')
        
        # Change xticks for readabilty
        g.set(xticks=np.arange(0,len(df_orientation['timestamp'].unique()),125))
        g.set(xticklabels=np.arange(0,df_orientation['timestamp'].to_numpy()[-1:]+1,5))
        
#        ax.set_ylim((-0.1, self.dfOverlaps['dist'].max()+0.1+10*self.dfOverlaps['dist'].max()/100))
#
#        plt.legend([ 'obj_'+str(int(x)) for x in list_id_obj])   
         
        
    def display_overlaps_stats_objects(self, list_id_obj=None, ax=None):
        """
        Visualisation of overlaps of list objects per frame
        """

        assert not self.dfOverlaps.empty, "Overlaps dataframe is empty"
                       
        # Creation of figure
        if ax is None:
            fig = plt.figure(figsize=(10, 5))
            ax = fig.add_subplot(111)

        # With seaborn package
        
        df_overlap = self.dfOverlaps.copy()
        df_overlapTP = self.dfOverlaps.copy()
        
        df_overlap['overlaptype'] = ['overlap']*df_overlap.shape[0]
        df_overlap['overlap%'] = df_overlap['overlap']*100
        
        df_overlapTP['overlaptype'] = ['overlapTP']*df_overlapTP.shape[0]
        df_overlapTP['overlap%'] = df_overlap['overlapTP']*100
        
        df_overlap_stats =  df_overlap.append(df_overlapTP, ignore_index=True)
        
        df_overlap_stats.id_GT = df_overlap_stats.id_GT.astype('int')
        
        sns.set_style("whitegrid", {'axes.grid' : False})
        
        # Draw a nested boxplot to show bills by day and time
        sns.boxplot(x="id_GT", y="overlap%",
                    hue="overlaptype", palette=["m", "c"],
                    data=df_overlap_stats)


#       # With matplotlib        
#        # All GT objects
#        if list_id_obj is None:     
#            list_id_obj = self.dfOverlaps['id_GT'].unique()
#        
#        # Creation of figure
#        if ax is None:
#            fig = plt.figure(figsize=(10, 5))
#            ax = fig.add_subplot(111)
#                        
#        mean = np.array([])
#        std = np.array([])
#        
#        meanTP = np.array([])
#        stdTP = np.array([])
#        
#        for (idx, id_obj) in enumerate(list_id_obj):
#            
#            df_overlap_obj = self.dfOverlaps[self.dfOverlaps['id_GT'] == id_obj]['overlap']
#        
#            mean = np.append(mean, df_overlap_obj.mean())
#            std = np.append(std, df_overlap_obj.std())
#            
#            df_overlap_obj_TP = self.dfOverlaps[self.dfOverlaps['id_GT'] == id_obj]['overlapTP']
#        
#            meanTP = np.append(meanTP, df_overlap_obj_TP.mean())
#            stdTP = np.append(stdTP, df_overlap_obj_TP.std())
#                    
#        plt.errorbar(list_id_obj, mean*100, std*100, linestyle='None', marker='s', mfc = 'm', mec = 'm', ecolor = 'm', elinewidth = 2, capsize = 5)
#        
#        plt.errorbar(list_id_obj, meanTP*100, stdTP*100, linestyle='None', marker='o', mfc = 'c', mec = 'c', ecolor = 'c', elinewidth = 2, capsize = 5)
#        
        ax.set_title("Overlaps mean and std of GT objects for a sequence")
#        ax.set_xlabel('GT Objects')
#        ax.set_ylabel('Overlap %')
#               
#        ax.set_ylim((-0.2, 103))
#     
#        plt.xticks(np.arange(min(list_id_obj), max(list_id_obj)+1))
#        
#        plt.legend(["overlap", "overlapTP"], loc=4)
        
        
    def display_distances_stats_objects(self, list_id_obj=None, ax=None):
        """
        Visualisation of overlaps of list objects per frame
        """

        assert not self.dfOverlaps.empty, "Overlaps dataframe is empty"
        
        # Creation of figure
        if ax is None:
            fig = plt.figure(figsize=(10, 5))
            ax = fig.add_subplot(111)
        
        df_distance_stats = self.dfOverlaps.copy()
        df_distance_stats.id_GT = df_distance_stats.id_GT.astype('int')
        
        # With seaborn
        sns.boxplot(x="id_GT", y="dist",
                    data=df_distance_stats)
        
        
#        # With matplotlib
#        
#        # All GT objects
#        if list_id_obj is None:     
#            list_id_obj = self.dfOverlaps['id_GT'].unique()
#                        
#        mean = np.array([])
#        std =  np.array([])
#        
#        for (idx, id_obj) in enumerate(list_id_obj):
#            
#            df_overlap_obj = self.dfOverlaps[self.dfOverlaps['id_GT'] == id_obj]['dist']
#        
#            mean = np.append(mean, df_overlap_obj.mean())
#            std = np.append(std, df_overlap_obj.std())
#        
#        plt.errorbar(list_id_obj, mean, std, linestyle='None', marker='o', mfc = 'b', mec = 'b', ecolor = 'b', elinewidth = 2, capsize = 5)
#        
#        ax.set_xlabel('GT Objects')
#        
#        plt.xticks(np.arange(min(list_id_obj), max(list_id_obj)+1))
#        
#        ax.set_ylim((-0.2, mean.max()+1))
        
        ax.set_ylabel('Distances (m)')
        
        ax.set_title("Distances mean and std of GT objects for a sequence")
        
        
    def display_inclusions_by_frame(self, ax=None):
        """
        Visualisation of inclusions per frame
        """

        if self.dfInclusions.empty:
            print("No inclusions to display\n")
            if ax is not None:
                ax.set_title("No inclusion")
                
                # Turn off axis lines and ticks of the big subplot
                ax.spines['top'].set_color('none')
                ax.spines['bottom'].set_color('none')
                ax.spines['left'].set_color('none')
                ax.spines['right'].set_color('none')
                ax.tick_params(labelcolor='w', top=False, bottom=False, left=False, right=False)
                
            return
        
        # Creation of figure
        if ax is None:
            fig = plt.figure(figsize=(10, 5))
            ax = fig.add_subplot(111)
        
        print(self.dfInclusions)  
        
        print(self.vInclusions)
        
        v_time = [(timestamp - self.first_timestamp)/1000 for timestamp in self.seq.get_frames().keys()]
                      
        plt.plot(v_time, self.vInclusions, color='r', marker='*', linestyle='None')
        
        ax.set_title("Number of inclusions by frame")
        ax.set_xlabel('Time (s)')
       
        ax.set_ylim((-0.2, max(self.vInclusions)+1))
 
        plt.xticks(v_time)
        
        plt.yticks(np.arange(0, max(self.vInclusions)+1, step=1))
        
        
    def display_inclusions_objects_by_frame(self, list_id_obj=None, ax=None):
        """
        Visualisation of inclusions of objects per frame
        """
        
        if self.dfInclusions.empty:
            print("No inclusions to display\n")
            if ax is not None:
                ax.set_title("No inclusion")
                
                # Turn off axis lines and ticks of the big subplot
                ax.spines['top'].set_color('none')
                ax.spines['bottom'].set_color('none')
                ax.spines['left'].set_color('none')
                ax.spines['right'].set_color('none')
                ax.tick_params(labelcolor='w', top=False, bottom=False, left=False, right=False)
                
            return
        
        # Creation of figure
        if ax is None:
            fig = plt.figure(figsize=(10, 5))
            ax = fig.add_subplot(111)
        
        # All GT objects
        if list_id_obj is None:     
            list_id_obj = self.dfInclusions['id_GT'].unique()
              
        # Colors of objects
        v_colors = plt.cm.get_cmap('hsv', len(list_id_obj)+1)
        
        # Markers of objects        
        v_markers = ["+", "x", "o", "s", ">", "<", "v", "^"]
       
        for (idx, id_obj) in enumerate(list_id_obj):
            
             df_inclusion_obj = self.dfInclusions[self.dfInclusions['id_GT'] == id_obj]
             
             plt.plot((df_inclusion_obj['timestamp'].to_numpy() - self.first_timestamp)/1000, np.ones(df_inclusion_obj.shape[0]), 
                      color=v_colors(idx), marker=v_markers[idx], fillstyle='none', linestyle='None')
            
        ax.set_title("Inclusion by object per frame : {} inclusions, first at {}s".format(sum(self.vInclusions), self.time_first_inclusion))
        ax.set_xlabel('Time (s)')
       
        last_timestamp = max(list(self.seq.get_frames().keys()))
        
        ax.set_xlim((0, (last_timestamp-self.first_timestamp)/1000 ))  
        ax.set_ylim((0, 1.5))  
        
        plt.yticks([0, 1])
        
        plt.legend([ 'obj_'+str(int(x)) for x in list_id_obj])    
        
        
    def display_distances_from_ego_by_frame(self, ax=None):
        """
        Visualisation of objects distances from ego for each frame
        """
        
        df_gt_objs = pd.DataFrame([], columns=['timestamp', 'id_GT', 'dist2ego'])
        
        # Compute for all objects distances to ego
        for timestamp in self.seq.get_frames().keys(): 
            
            oFrame =  self.seq.get_frame(timestamp)
            
            df_gt_by_frame = oFrame.get_gt_df_objects()
           
            df_gt_by_obj = pd.DataFrame([], columns=['timestamp', 'id_GT', 'dist2ego'])
      
            df_gt_by_obj['timestamp'] = [timestamp] * df_gt_by_frame.shape[0]
            
            df_gt_by_obj['id_GT'] = df_gt_by_frame[frame_cst.ID]
    
            df_gt_by_obj['dist2ego'] = np.sqrt(df_gt_by_frame[frame_cst.X]**2 + df_gt_by_frame[frame_cst.Y]**2)
                
            df_gt_objs = df_gt_objs.append(df_gt_by_obj, ignore_index=True)
        
        # Creation of figure
        if ax is None:
            fig = plt.figure(figsize=(10, 5))
            ax = fig.add_subplot(111)
            
        df_gt_objs['timestamp'] = (df_gt_objs['timestamp'].to_numpy() - self.first_timestamp)/1000
        df_gt_objs['id_GT'] = df_gt_objs['id_GT'].astype('int')
        
        uniqueGTId = df_gt_objs['id_GT'].unique()
        
        sns.set_style("whitegrid", {'axes.grid' : False})
        
        # Draw a nested boxplot to show bills by day and time
        sns.lineplot(x="timestamp", y="dist2ego", hue="id_GT", data=df_gt_objs, palette=sns.color_palette("bright", len(uniqueGTId)))
        
        # All GT objects    
        list_id_obj = df_gt_objs['id_GT'].unique()
#              
#        # Colors of objects
#        v_colors = plt.cm.get_cmap('hsv', len(list_id_obj)+1)
#       
#        for (idx, id_obj) in enumerate(list_id_obj):
#            
#             df_gt_by_obj = df_gt_objs[df_gt_objs['id_GT'] == id_obj]
#                         
#             plt.plot((df_gt_by_obj['timestamp'].to_numpy() - self.first_timestamp)/1000, df_gt_by_obj['dist2ego'].to_numpy(), 
#                      color=v_colors(idx), linestyle='-') #marker='o', fillstyle="none",
        
        ax.set_title("Distances between ego and GT objects by frame")
        ax.set_xlabel('Time (s)')
        ax.set_ylabel('Distances (m)')
        
#        ax.set_ylim((0,  df_gt_objs['dist2ego'].max()+5))
#          
        # CLasses list of GT object 
        l_class=[]
        lsort = np.sort(list_id_obj)
        for id_obj in lsort:
            value_classid =  self.seq.get_gt_obj_class(id_obj)
            l_class.append( ClassGT(value_classid ).name)
#        
        plt.legend(['obj_' + str(int(lsort[idx])) + ' : ' + l_class[idx] for idx in range( len(l_class))], loc=4)   
        
#        self.dfClassification = pd.DataFrame([], columns=['timestamp', 'id_GT', 'class_GT', 'id_LT', 'class_LT'])      
        
        
    def display_metrics(self, path_name='Visu_Metrics.png'):
        """
        Visualisation of some metrics :
            * Distances between ego and GT objects
            * Overlaps TP by object by frame
            * Overlaps TP and all stats by objects 
            * Distances TP by object by frame
            * Distances stats by objects
            * Inclusion by object by frame
            
        Input :
            path_name : path and name of the image to save 
            
        """
        
        # Creation of figure
        fig = plt.figure(figsize=(16, 8.5))
        
        
        # Visualisation distances objects from ego
        ax = fig.add_subplot(321)
        self.display_distances_from_ego_by_frame(ax=ax)
        
        
        # Visualisation inclusion bu objets by frame
        ax = fig.add_subplot(322)
        self.display_orientionserr_objects_by_frame(ax=ax)
        #self.display_inclusions_objects_by_frame(ax=ax)
        
        
        # Visualisation overlaps by object by frame
        ax = fig.add_subplot(323)
        self.display_overlaps_objects_by_frame(ax=ax)
        
        
        # Visualisation stats on overlap 
        ax = fig.add_subplot(324)
        self.display_overlaps_stats_objects(ax=ax)
        
        
        # Visualisation distances by object by frame
        ax = fig.add_subplot(325)
        self.display_distances_objects_by_frame(ax=ax)
        
        
        # Visualisation stats on distances
        ax = fig.add_subplot(3,2,6)
        self.display_distances_stats_objects(ax=ax)
        
        plt.tight_layout()
        
        # Record
        plt.savefig(path_name, dpi=300)
              
        plt.close(fig)