# -*- coding: utf-8 -*-
"""
@Filename : sequence

Description : model a sequence

Created on mai 17, 2019 at 11:02:20

@author: thomas.bardot
"""
from validlt.computemetrics.objectscomparison.frame import Frame
import numpy as np


class Sequence:
    def __init__(self, frames=[]):
        """
        Constructor
        Args:
            frames(Frame[]): list of frame objects.
        """

        # dictionary which contains all of the frames {frame_timestamp : Frame}
        self.__frames = {}
        # dictionary which contains the class of each gt object {gt_obj_id : gt object class}. Contains None if the
        # class cannot be determined
        self.__gt_class = {}
        # dictionary which associates to the id of gt object the list of frames timestamp which contain the object
        # {gt_obj_id : list(timestamp)}.
        self.__gt_frames = {}
        # dictionary which associates to the frames timestamp of the current sequence, the corresponding timestamp of
        # the sequence from which the current object has been created during a merge operation. Empty if not created
        # with a merge operation
        self.merged_timestamps = {}

        self.add_frames(frames)

    def add_frame(self, frame_to_add):
        """
        Add a frame object to the frame dictionary of the sequence
        Args:
            frame_to_add[Frame): frame object
        """
        assert frame_to_add is not None and isinstance(frame_to_add, Frame), "Objects must be Fralme type"
        self.__frames[frame_to_add.timestamp] = frame_to_add
        self.construct_gt_class_dict()
        self.construct_gt_frames()

    def add_frames(self, frames_to_add):
        """
        Add a list of frames to the Sequence
        Args:
            frames_to_add(Frame[]): a list of frame to add to the sequence
        """
        for frame in frames_to_add:
            assert frame is not None and isinstance(frame, Frame), "Objects must be Fralme type"
            self.__frames[frame.timestamp] = frame
        self.construct_gt_class_dict()
        self.construct_gt_frames()

    def construct_gt_frames(self):
        """
        Construct the attribute self.__gt_frames {gt_obj_id : list(timestamp)} for all the sequence

        Returns:

        """
        for frame in self.__frames.values():
            for gt_obj_id in frame.gt_objects.keys():
                if gt_obj_id in self.__gt_frames.keys():
                    if frame.timestamp in self.__gt_frames[gt_obj_id]:
                        None
                    else:
                        self.__gt_frames[gt_obj_id].append(frame.timestamp)
                else:
                    self.__gt_frames[gt_obj_id] = [frame.timestamp]

    def construct_gt_class_dict(self):
        """
        Construct the attribute self.__gt_class {gt_obj_id : gt object class} for all the sequence
        Returns:

        """
        for frame in self.__frames.values():
            for gt_obj_id in frame.gt_objects.keys():
                if gt_obj_id in self.__gt_class.keys():
                    if self.__gt_class[gt_obj_id] == None or \
                            self.__gt_class[gt_obj_id] != frame.gt_objects[gt_obj_id].obj_type:
                        self.__gt_class[gt_obj_id] = None
                    else:
                        None
                else:
                    self.__gt_class[gt_obj_id] = frame.gt_objects[gt_obj_id].obj_type

    def reset(self):
        """
        Clear the frame dictionary
        """
        self.__frames.clear()
        self.__gt_class.clear()
        self.__gt_frames.clear()

    def get_frames(self):
        """
        Get the dictionary which contains all the frame objects of the sequence.
        Key is the frame timestamp.
        Returns: the dictionary of all frames
        """
        return self.__frames

    def get_frames_contain_gt(self, gt_obj_id):
        """
        Return the list timestamp of frames which contain the gt object passed in argument.
        If a frame of the sequence has been modified outside the sequence and has not been added with self.add_frame
        or self.add_frames, then the method self.construct_gt_frames must be called before
        getting the gt object list of frame timestamp

        Args:
            gt_obj_id: id of a ground truth tracked object

        Returns: list of dataframe timestamp which contains the gt object passed in argument

        """
        if gt_obj_id in self.__gt_frames:
            return self.__gt_frames[gt_obj_id]
        else:
            return []

    def get_gt_obj_class(self, gt_obj_id):
        """
        Return the class of a gt object.
        If a frame of the sequence has been modified outside the sequence and has not been added with self.add_frame
        or self.add_frames, then the method self.construct_gt_class_dict must be called before
        getting the gt object class

        Args:
            gt_obj_id:  id of a ground truth tracked object

        Returns: the class of the gt object passed in argument. Return None if the object class is not found

        """
        if gt_obj_id in self.__gt_class:
            return self.__gt_class[gt_obj_id]
        else:
            return None

    def get_frame(self, timestamp):
        """
        Args:
            timestamp: timestamp of a frame

        Returns: the frame which has the same timestamp than the one passed in argument. Return None if there is
        no frame with the given timestamp
        """
        frame = None
        if timestamp in self.__frames:
            frame = self.__frames[timestamp]

        return frame

    def get_nearest_timestamp(self, timestamp):
        """
        Args:
            timestamp: timestamp of a frame

        Returns: the nearest timestamp if the timestamp passed in argument does not exist in current Sequence.
        """
        nearest_timestamp = timestamp
        timestamps_list = list(self.__frames.keys())
        if nearest_timestamp not in timestamps_list:
            nearest_timestamp = min(timestamps_list, key=lambda x: abs(x - timestamp))

        return nearest_timestamp

    @staticmethod
    def merge_sequence(sequence_1, sequence_2, period=None):
        """
        Merge the two sequence in one. The new sequence will be based on the timestamps of the sequence_1.
        When merging a sequence which contains only LT with a sequence which contains only GT objects, then the GT
        sequence shall be the sequence_1.
        Args:
            sequence_1(Sequence): first sequence to merge. The new sequence will be based on its timestamps
            sequence_2(Sequence): second sequence to merge
            period: maximal time gap to synchronise timestamp between sequence_1 and sequence_2 in millisecond

        Returns: the resulting sequence of the merge operation
        """
        assert isinstance(sequence_1, Sequence) and isinstance(sequence_2, Sequence)
        merged_sequence = Sequence()

        merged_timestamp = {}
        
        # Compute the period between timestamp, maximal gap is 75% of one period
        if period is None:
            period = np.mean(np.diff(list(sequence_1.get_frames().keys()))) * 3/4

        for timestamp in sequence_1.get_frames().keys():
            # For each frame of first sequence, find the corresponding frame in the second sequence and create
            # a merged sequence
            timestamp_2 = sequence_2.get_nearest_timestamp(timestamp)
            
            # Check if gap between timestamps respects maximal period
            if abs(timestamp_2-timestamp) < period:
            
                merged_timestamp[timestamp] = timestamp_2
                merged_sequence.add_frame(Frame.merge_frame(sequence_1.get_frame(timestamp),
                                                            sequence_2.get_frame(timestamp_2),
                                                            timestamp))
            else:
                merged_timestamp[timestamp] = -1
                merged_sequence.add_frame(Frame.merge_frame(sequence_1.get_frame(timestamp),
                                                            Frame(-1),
                                                            timestamp))                        
 
        merged_sequence.merged_timestamps = merged_timestamp
        
        return merged_sequence

if __name__ == '__main__':
    from validlt.computemetrics.objectscomparison import TrackedObject
    from validlt.computemetrics.objectscomparison.oriented_bounding_box import OrientedBoundingBox

    # How to use sequence
    timestamp = 0.40
    test_obj_1 = TrackedObject(1, OrientedBoundingBox(1, 2, 4, 5, 3), 1)
    test_obj_2 = TrackedObject(2, OrientedBoundingBox(2, 2, 4, 5, 3), 1)
    frame1 = Frame(timestamp)
    frame1.add_lt_objects([test_obj_1, test_obj_2])
    frame2 = Frame(timestamp+40)
    sequence = Sequence([frame1, frame2])
