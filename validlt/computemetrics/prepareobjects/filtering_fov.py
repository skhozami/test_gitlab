# -*- coding: utf-8 -*-
"""
Created on Fri Oct  4 09:10:14 2019

@author: sarah.le-bagousse
"""

import shapely.geometry as sg
import numpy as np
from validlt.computemetrics.objectscomparison.oriented_bounding_box import OrientedBoundingBox

def compute_fov(range_max=150, ang_fov=145, pos_lidar = [0,0]):
    """
    description : compute the lidar fov
    
    args:
        range_max : length of the lidar range
        ang_fov : angle of the lidar FOV in degrees
        
    returns:
        fov : shapely object of the lidar FOV    
    """
    
    # Parameters
    ang_rad = (ang_fov / 2) * np.pi / 180
    posx_lidar = pos_lidar[0]
    posy_lidar = pos_lidar[1]
    
    # Circle including lidar FOV
    circle = sg.Point(posx_lidar, posy_lidar).buffer(range_max)
    
    # Triangle including FOV
    xT = range_max
    yT = np.sqrt((xT/np.cos(ang_rad))**2 - xT**2)
    
    triangle = sg.Polygon([(posx_lidar, posy_lidar), (xT, -yT), (xT, yT)])
    
    # FOV
    return triangle.intersection(circle)


def check_obj_in_fov(oBB, fov):
    """
    description : compute the lidar fov
    
    args:
        oBB : Object to check
        fov : shapely object of the lidar FOV 
        
    returns:
        bInFOV : indicator if object si in th fov
                    0 : outside
                    1 : part is in
                    2 : totally inside
    """
    
    # Initialisation
    bInFOV = 0 # outside the fov
    
    # Check intersection between fov and obb
    InFOV = fov.intersection(oBB.get_contour())
    
    InFOV_area = np.round(InFOV.area, 1)
    
    # part is inside
    if InFOV_area == 0:
        bInFOV = 0 
     
    # part of object is inside FOV lear from ego
    elif InFOV_area < np.round(oBB.get_contour().area, 1) and oBB.cx < 100:
        bInFOV = 1
    
    # rear of object is inside FOV
    else:
        bInFOV = 2

    return bInFOV


#%%
if __name__ == '__main__':
    
    # FOV 
    fov = compute_fov()
    
    # check object  
    oBB = OrientedBoundingBox(-10, -3.5, 4, 2, 0.5)
    print("outside fov")
    print(check_obj_in_fov(oBB, fov))
 
    oBB = OrientedBoundingBox(1, -2, 4, 2, 0.5)
    print("on fov")  
    print(check_obj_in_fov(oBB, fov))

    oBB = OrientedBoundingBox(10, -3.5, 4, 2, 0.5)
    print("inside fov")
    print(check_obj_in_fov(oBB, fov))
      
        
        
