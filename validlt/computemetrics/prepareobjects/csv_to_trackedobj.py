# -*- coding: utf-8 -*-
"""
csv_to_tracked_obj - convert object in .csv format into Python format

@author: Thomas Bardot
"""
from validlt.computemetrics.objectscomparison.oriented_bounding_box import OrientedBoundingBox
from validlt.computemetrics.objectscomparison.tracked_object import TrackedObject
from validlt.computemetrics.objectscomparison.sequence import Sequence
from validlt.computemetrics.objectscomparison.frame import Frame
from validlt.computemetrics.prepareobjects.filtering_fov import compute_fov
import validlt.computemetrics.constants as cst
import pandas as pd
import os as os


class CsvToTrackedObjects:
    @staticmethod
    def __convert_to_obj(row, fov=None):
        """
        Convert the row passed in argument into a TrackedObject
        Args:
            row: a dataframe row
            fov: shalepy object of the FOV
            
        Returns:
        """
        obj_id = row[cst.CSV_ID]
        obj_type = row[cst.CSV_OBJTYPE]
        x_coord = row[cst.CSV_XCOORD]
        y_coord = row[cst.CSV_YCOORD]
        width = row[cst.CSV_WIDTH]
        length = row[cst.CSV_LENGTH]
        yaw = row[cst.CSV_YAW]
        bounding_box = OrientedBoundingBox(x_coord, y_coord, length, width, yaw)
                
        id_fov = bounding_box.apply_fov(fov)

        tracked_obj = TrackedObject(obj_id, bounding_box, obj_type, id_fov)
        return tracked_obj
    
    @staticmethod
    def __convert_to_sequence(csv_file_path, is_gt_obj, fov):
        """
        Converst a .csv file into a sequence object
        Args:
            csv_file_path: path of the csv file
            is_gt_obj: True if the sequence contains only gt objects

        Returns:

        """
        sequence = Sequence()
        seq_dataframe = pd.read_csv(csv_file_path, delimiter=";", header=0, encoding='ascii')

        if not is_gt_obj:
            # Iter on dataframe rows
            for index, row in seq_dataframe.iterrows():
                timestamp = row[cst.CSV_TIMESTAMP]
                tracked_obj = CsvToTrackedObjects.__convert_to_obj(row, fov=fov)
                if sequence.get_frame(timestamp) is not None:
                    sequence.get_frame(timestamp).add_lt_object(tracked_obj)
                else:
                    frame = Frame(timestamp)
                    frame.add_lt_object(tracked_obj)
                    sequence.add_frame(frame)
        else:
            # Iter on dataframe rows
            for index, row in seq_dataframe.iterrows():
                timestamp = row[cst.CSV_TIMESTAMP]
                tracked_obj = CsvToTrackedObjects.__convert_to_obj(row, fov=fov)
                if sequence.get_frame(timestamp) is not None:
                    sequence.get_frame(timestamp).add_gt_object(tracked_obj)
                else:
                    frame = Frame(timestamp)
                    frame.add_gt_object(tracked_obj)
                    sequence.add_frame(frame)

        return sequence
    
    @staticmethod
    def __make_dataframe(sequence):
        """
        Construct the dataframe for all frames of the sequence
        passed in argument
        Args:
            sequence: a sequence of frames

        Returns:

        """
        for timestamp in sequence.get_frames():
            sequence.get_frame(timestamp).construct_dataframes()
    
    @staticmethod
    def convert_all(gt_csv_file_path, lt_csv_file_path, period=None):
        """
        Args:
            gt_csv_file_path: path of the csv file which contains the gt objects
            lt_csv_file_path: path of the csv file which contains the lt objects

        Returns: a sequence object which contains all frame filled with gt and lt objects
        """
        assert os.path.isfile(gt_csv_file_path), "Directory '{}' not existing".format(gt_csv_file_path)
        assert os.path.isfile(lt_csv_file_path), "Directory '{}' not existing".format(lt_csv_file_path)
        
        # Compute FOV
        fov = compute_fov(range_max=cst.FOV_RANGE, ang_fov=cst.FOV_ANGDEG, pos_lidar=cst.FOV_POSLIDAR)
        
        sequence_lt = CsvToTrackedObjects.__convert_to_sequence(lt_csv_file_path, False, fov)
        sequence_gt = CsvToTrackedObjects.__convert_to_sequence(gt_csv_file_path, True, fov)
        
        sequence = Sequence.merge_sequence(sequence_gt, sequence_lt, period=period)
        
        CsvToTrackedObjects.__make_dataframe(sequence)
        
        return sequence
