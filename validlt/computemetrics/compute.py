# -*- coding: utf-8 -*-
"""
@Filename : validlt

Description : This module is the entry point of the core of validlt tool.
It compares a .csv file which contains the ground truth objects with a .csv file
which contains the lidar tracker objects. It then generates reports.

Created on juil. 10, 2019 at 10:38:47

@author: thomas.bardot
"""

from validlt.utils import get_string_date
from validlt.utils import make_zip
from validlt.computemetrics.prepareobjects.csv_to_trackedobj import CsvToTrackedObjects
from validlt.computemetrics.objectscomparison.sequence_metrics import SequenceMetrics
from validlt.computemetrics.resultsanalysis.metrics_analyzer import MetricsAnalyzer
from validlt.computemetrics.resultsanalysis.reports_generator import ReportsGenerator
import os.path
import sys
import datetime


def run_validlt(gt_csv, lt_csv, output_file, period, version_LT, test_date):
    """
    Entry point of the validlt tool
    Args:
        gt_csv: full path of the gt_csv file
        lt_csv: full path of the lt_csv file
        output_file: full path of the output file file
        period: maximum time period to synchronise two frames in millisecond
        version_LT: version of the lidar tracker
        test_date: date of the launch of the test. This date permits to identify the test session and give unique name
        to generated files

    Returns: 0 if success

    """

    string_date = get_string_date(test_date)
    os.getcwd()
    try:
        # Setup tool
        output_dir = os.path.dirname(output_file)
        print(f"Output repository: {os.path.abspath(output_dir)}")

        print(f"\n{'':#<80}\n")
        print(f"Test on  files: \n- LT: {lt_csv}\n- GT: {gt_csv}")

        # 1- Prepare all GT and LT objects
        print(f"{'1. Generate all GT and LT objects ':.<80}")
        sequence = prepare_tracked_objects(lt_csv, gt_csv, period=period)

        # 2- Generate metrics
        print(f"{'2. Generate metrics ':.<80}")
        metrics, metrics_overall_img, confusion_img = compute_metrics(sequence, output_dir, version_LT)

        # 3- Analyze metrics and generate reports
        print(f"{'3. Analyze metrics and generate reports ':.<80}")
        detailed_report = analyze_metrics(metrics, output_dir, test_date, metrics_overall_img, confusion_img, version_LT)

        # 4- Construct archive output
        print(f"{'4. Construct archive output ':.<80}")
        zip_file = construct_archive([detailed_report, metrics_overall_img, confusion_img], output_file)

        # 5- Remove temporary files
        print(f"{'5. Remove temporary files ':.<80}")
        for temp_file in [detailed_report, metrics_overall_img, confusion_img]:
            os.remove(temp_file)
            
#        # 6- KPI analyses
#        print(f"{'6. KPI metrics':.<80}")
#        analyze_kpi(metrics, output_dir, string_date)

        print(f"\n 1 file has been generated: \n {zip_file}\n")
        print(f"\nThe ValidLT tool has been successfully launched.\n\n")

    except Exception as e:
        print(f"\nError occurs during a test. Aborting.\n{e}")
        sys.exit(1)
    return 0


def prepare_tracked_objects(lt_file_path, gt_file_path, period=None):
    """
    Construct the sequence object to analyze

    Args:
        lt_file_path(String): path for the .csv file which contains the lidar tracker objects
        gt_file_path(String): path for the .csv file which contains the ground truth objects
        period :

    Returns: the sequence object to analyze

    """
    try:
#        csv_converter = CsvToTrackedObjects()
        sequence = CsvToTrackedObjects.convert_all(gt_file_path, lt_file_path, period=period)
        return sequence
    except Exception as e:
        raise Exception(f"Error occurs when preparing the tracked objects. \n{e}")


def compute_metrics(sequence, output_dir, version_LT):
    """
    Compute all metrics for the sequence and all frames contained in it

    Args:
        sequence(Sequence): the sequence to analyze
        output_dir: repository where the reports have to be generated

    Returns: a SequenceMetrics object and the path to the metrics figure

    """
    try:
        metrics_figure_path = f"overall_{version_LT}.png"
        metrics_figure_path = os.path.join(output_dir, metrics_figure_path)
        confusion_matrix_img = f"confusion_{version_LT}.png"
        confusion_matrix_img = os.path.join(output_dir, confusion_matrix_img)
        sequence_metrics = SequenceMetrics(sequence, output_dir, maxD=1)
        sequence_metrics.compute_metrics()
        # print(f"DEBUG: Display metrics")  # DEBUG
        sequence_metrics.display_metrics(metrics_figure_path)
        sequence_metrics.display_confusion_matrix(confusion_matrix_img)
        # print(f"DEBUG: End of Display metrics")  # DEBUG
        sequence_metrics.save_df_kpi(version_LT=version_LT)
        return sequence_metrics, metrics_figure_path, confusion_matrix_img
    except Exception as e:
        raise Exception(f"Error occurs during metrics computation. \n{e}")


def analyze_metrics(metrics, output_dir, test_date, metrics_overall_img, confusion_img, lt_version):
    """
    Analyze metrics and generate reports

    Args:
        metrics: computed metrics on lidar tracking
        output_dir: repository where the reports have to be generated
        test_date: date when the tool has been launched
        metrics_overall_img : path to the figure of the test
        confusion_img : path of the figure with the confusion matrix

    Returns: detailed report

    """
    try:
        metrics_analyser = MetricsAnalyzer(metrics)
        metrics_analyser.analyze()
        report_generator = ReportsGenerator(metrics_analyser, metrics_overall_img, confusion_img, test_date, lt_version)
        summary_report, detailed_report = report_generator.gen_reports(output_dir)
    except Exception as e:
        raise Exception( f"Error occurs when analyzing metrics. \n{e}")

    print(f"New detailed report:\n- {detailed_report}")
    return detailed_report


def construct_archive(files, output_filename):
    """
    Construct an archive files which contains all the files passed in parameter

    Args:
        files: list of file to add to the archive
        output_filename: where to archive has to be generated

    Returns: the path of the .zip archive file

    """
    try:
        zip_file = output_filename if output_filename.endswith('.zip') else output_filename + '.zip'
        make_zip(files, zip_file)
        return zip_file
    except Exception as e:
        raise Exception(f"Error occurs when constructing .zip archive file. \n{e}")
   
     
#def  analyze_kpi(metrics, output_dir, date):
#    """
#    Analyze kpi
#
#    Args:
#        metrics: computed metrics on lidar tracking
#        output_dir: repository where the reports have to be generated
#        date: test date in string format
#
#    Returns:
#    
#    """
#    try:
#        kpi_analyser = KpiAnalyzer(metrics, output_dir, str_date=date, thres_overlap=50, thres_overlapLT=75, ang_resol=0.25)
#        
#        kpi_analyser.compute_tab_metrics()
#                
#        kpi_analyser.save_tab_metrics()
#
#    except Exception as e:
#        raise Exception( f"Error occurs when analyzing kpi. \n{e}")


if __name__ == '__main__':
    ####### Uncomment this part when doing release version ########
    datetime.now()

    path = "\\\\acsysteme.lan\\dfs\\data\\Renault\\Fusion\\Validation_Lidar\\DataValidLT\\Capsules_SimObject\\"

    l_capsules = ['capsule_sc_3L_ego19ms_1Car_1Truck_1Motorbike',
                  'capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbike',
                  'capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbikebehind',
                  'capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbikebehind_Lbend',
                  'capsule_sc_3L_ego30ms_1Car_2Truck_2Motorbike',
                  'capsule_sc_3L_ego30ms_1Car_2Truck_2Motorbike_Rbend',
                  'capsule_sc_3L_ego30ms_2Car_1Truck',
                  'capsule_sc_3L_ego36ms_2Car_1Truck_1Motorbike',
                  'capsule_sc_3L_ego36ms_2Car_1Truckfront_1Motorbike']

    version_LT = "v3"

    for name_capsule in l_capsules:

        path_capsule = path + name_capsule

        out = path_capsule + "\\resultats04septembre2019"

        pathGT = path_capsule + '\\GT\\SimObjs_GT.csv'

        pathLT = path_capsule + '\\LT_outputs\\Objs_LT.csv'

        # cmd_line_args = ['-o', out, '-v', version_LT, pathGT, pathLT]

        # run_validlt(cmd_line_args)
        run_validlt(pathGT, pathLT, out, None, version_LT, date)


    