# -*- coding: utf-8 -*-
"""
@Filename : constants

Description :

Created on mai 20, 2019 at 10:17:24

@author: thomas.bardot
"""

# VERSION = "0.1"

# For debugging
DEBUG = True
CONFIG_FILENAME = "validlt_config_test_LT.json"

# DEBUG_GT_OBJ_FILENAME = "ObjsGT_Deviation.csv"
# DEBUG_LT_OBJ_FILENAME = "ObjsLT_Deviation.csv"

# Description of the json config file keys
JSON_OUTPUT_DIR = "result_directory"
JSON_TESTSDICT = "tests"
JSON_TEST_DESCR = "description"
JSON_TEST_SCNID = "scenario_id"
JSON_TEST_CONFID = "config_id"
JSON_TEST_CAPSPATH = "capsule_path"
JSON_TEST_GTFILE = "gt_obj_file"
JSON_TEST_LTFILE = "lt_obj_file"
JSON_RUN_LT = "run_lidar_tracker"

# description of the input .csv format
CSV_TIMESTAMP = 'timestamp(ms)'
CSV_ID = 'id'
CSV_OBJTYPE = 'class'
CSV_XCOORD = 'posx(m)'
CSV_YCOORD = 'posy(m)'
CSV_WIDTH = 'width(m)'
CSV_LENGTH = 'length(m)'
CSV_YAW = 'yaw(rad)'

# parameters of field of view
FOV_RANGE = 150 # in meters
FOV_ANGDEG = 145 # in degree
FOV_POSLIDAR = [0,0]
