# -*- coding: utf-8 -*-
"""
@Filename : metrics_analyzer

Description : This module is used to analyze the metrics of a sequence and to generate a report

Created on mai 22, 2019 at 18:17:09

@author: thomas.bardot
"""
import os
import numpy as np


class MetricsAnalyzer:
    UNKNOWN = "unknown"
    N_A = "not applicable"

    def __init__(self, sequence_metrics):
        """
        Constructor

        Args:
            sequence_metrics: metrics of the tests to analyze

        """
        # self.test_scenario_id = scenario_id
        # self.test_config_id = config_id
        self.sequence_metrics = sequence_metrics
        # Attributes used to store the metrics analysis
        self.scenario = self.UNKNOWN
        self.config = self.UNKNOWN
        self.nb_frames = self.UNKNOWN
        self.nb_mostly_tracked_obj = self.UNKNOWN
        self.nb_mostly_lost_obj = self.UNKNOWN
        self.nb_fragmentations = self.UNKNOWN
        self.nb_misses = self.UNKNOWN
        self.false_positive = self.UNKNOWN
        self.switch = self.UNKNOWN
        self.mota = self.UNKNOWN
        self.motp = self.UNKNOWN
        self.nb_inclusion = self.UNKNOWN
        self.frame_index_first_inclusion = self.UNKNOWN

    def analyze(self):
        # TODO
        accumulator_name = "accMetrics"
        self.nb_frames = self.sequence_metrics.df_mot_metrics["num_frames"][accumulator_name]
        self.nb_mostly_tracked_obj = self.sequence_metrics.df_mot_metrics["mostly_tracked"][accumulator_name]
        self.nb_mostly_lost_obj = self.sequence_metrics.df_mot_metrics["mostly_lost"][accumulator_name]
        self.nb_fragmentations = self.sequence_metrics.df_mot_metrics["num_fragmentations"][accumulator_name]
        self.nb_misses = self.sequence_metrics.df_mot_metrics["num_misses"][accumulator_name]
        self.false_positive = self.sequence_metrics.df_mot_metrics["num_false_positives"][accumulator_name]
        self.switch = self.sequence_metrics.df_mot_metrics["num_switches"][accumulator_name]
        self.mota = self.sequence_metrics.df_mot_metrics["mota"][accumulator_name]
        self.motp = self.sequence_metrics.df_mot_metrics["motp"][accumulator_name]
        self.nb_inclusion = sum(self.sequence_metrics.vInclusions)
        self.frame_index_first_inclusion = np.nonzero(self.nb_inclusion)[0][0]+1 if self.nb_inclusion > 0 else self.N_A
