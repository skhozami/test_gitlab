# -*- coding: utf-8 -*-
"""
@Filename : reports_generator

Description : manage the generation of reports on metrics and kpi results

Created on juin 10, 2019 at 12:12:20

@author: thomas.bardot
"""
from validlt.utils import get_string_date
import xlsxwriter
import os


class ReportsGenerator:
    REPORT_PREFIX = "testReport_"
    TEXT_DELIMITER = "#########################################################################"
    SUMMARY_REPORT_NAME = "testReportSummary.txt"

    def __init__(self, metric_analyzer, overview_figure_path, confusion_img, date, lt_version = None):
        """
        Constructor

        Args:
            metric_analyzer: MetricAnalyzer object which contains the result of the metrics analyze
            overview_figure_path: path to the figure of the test
            confusion_img: path of the figure with the confusion matrix
            date: date when the tool has been launched for the test to analyze
            lt_version: version of the lidar tracker
        """
        self.metric_analyzer = metric_analyzer
        self.overview_figure_path = overview_figure_path
        self.confusion_img = confusion_img
        self.test_date = date
        self.lt_version = lt_version

    def gen_reports(self, output_dir):
        """
        Update the summary report and generate a detailed report for each test

        Args:
            output_dir: repository where the reports have to be generated

        Returns: summary report, detailed report

        """
        # TODO
        detailed_report = self.gen_xlsx_detailed_report(output_dir)
        summary_report = self.update_summary_report(output_dir)

        return summary_report, detailed_report

    def update_summary_report(self, output_path):
        # TODO
        is_new = False
        summary_report = None
        # # 1- get summary report name
        # report_path = os.path.join(output_path, self.SUMMARY_REPORT_NAME)
        # # 2- Create summary report if non existing
        # if not os.path.isfile(report_path):
        #     is_new = True
        # report =  open(report_path, 'w+')
        # # 3- Add header to summary if newly created
        # if is_new:
        #     header = f"{'':#<10}{'Summary Report'}"
        # 4- Add lines to summary report
        return summary_report

    def gen_xlsx_detailed_report(self, output_path):
        """
        Generate a detailed xlsx report for the current test

        Args:
            output_path: path where the detailed report will be stored

        Returns: the report file path

        """
        date = get_string_date(self.test_date)

        # 1- Set report name
        report_file_path = self._set_report_filename(output_path, True)
        workbook = xlsxwriter.Workbook(report_file_path)

        # 2- Fill overview worksheet
        worksheet = workbook.add_worksheet('Overview')
        worksheet.write('A1', 'Date')
        worksheet.write('A2', 'Scenario ID')
        worksheet.write('A3', 'Lidar Tracker version')
        worksheet.write('A4', 'Nb of frames analyzed')
        worksheet.write('A5', 'Nb of mostly tracked objects')
        worksheet.write('A6', 'Nb mostly lost objects')
        worksheet.write('A7', 'Nb of fragmentations')
        worksheet.write('A8', 'Nb of objects misses')
        worksheet.write('A9', 'Nb of false positive detection')
        worksheet.write('A10', 'Nb of switches')
        worksheet.write('A11', 'Nb of MOTA')
        worksheet.write('A12', 'Nb of MOTP')
        worksheet.write('A13', 'Nb of inclusions')
        worksheet.write('A14', 'First inclusion at frame')

        worksheet.write('B1', date)
        worksheet.write('B2', 'UNKNOWN')
        worksheet.write('B3', self.lt_version)
        worksheet.write('B4', self.metric_analyzer.nb_frames)
        worksheet.write('B5', self.metric_analyzer.nb_mostly_tracked_obj)
        worksheet.write('B6', self.metric_analyzer.nb_mostly_lost_obj)
        worksheet.write('B7', self.metric_analyzer.nb_fragmentations)
        worksheet.write('B8', self.metric_analyzer.nb_misses)
        worksheet.write('B9', self.metric_analyzer.false_positive)
        worksheet.write('B10', self.metric_analyzer.switch)
        worksheet.write('B11', self.metric_analyzer.mota)
        worksheet.write('B12', self.metric_analyzer.motp)
        worksheet.write('B13', self.metric_analyzer.nb_inclusion)
        worksheet.write('B14', self.metric_analyzer.frame_index_first_inclusion)

        worksheet.insert_image('D1', self.overview_figure_path)

        # 3- Format overview worksheet
        title_format = workbook.add_format({'bold': True, 'bg_color':'yellow'})
        data_format = workbook.add_format({'align': 'right', 'bg_color':'#F2F2F2'})
        worksheet.set_column('A:A', 30, title_format)
        worksheet.set_column('B:B', 30, data_format)
        worksheet.set_column('C:AB', cell_format=data_format)

        # 4- Fill class association worksheet
        worksheet = workbook.add_worksheet('Class association')
        worksheet.insert_image('D1', self.confusion_img)

        # 5- Format class association worksheet
        data_format = workbook.add_format({'align': 'right', 'bg_color': '#F2F2F2'})
        worksheet.set_column('A:Z', cell_format=data_format)

        # 6- Close workbook
        workbook.close()

        return report_file_path

    def gen_txt_detailed_report(self, output_path):
        """
        Generate a detailed text report for the current test

        Args:
            output_path: path where the detailed report will be stored

        Returns:

        """
        # 1- Set report name
        report_file_path = self._set_report_filename(output_path, False)

        # 2- Create report file
        assert os.path.isdir(output_path), f"Error: output directory '{output_path}' does not exist"
        report_file = open(report_file_path, "w")

        # 3- Fill the report file with the analyze result
        self.__fill_report(report_file)
        report_file.close()
        return report_file_path

    def __fill_report(self, report_file):
        """
        Fill the report file passed in parameter with the result of the metrics analyzer.

        Args:
            report_file: report file which shall have been opened in write mode

        Returns:

        """
        # TODO
        # 1- Scenario and config
        report_file.write(f"{self.TEXT_DELIMITER}\n")
        report_file.write(f"Test scenario: {self.metric_analyzer.scenario}     #####     "
                          f"Config: {self.metric_analyzer.config}\n")
        # 2- Metrics
        report_file.write(f"{self.TEXT_DELIMITER}\n")
        report_file.write("### Metrics ###\n")
        report_file.write(f"Number of frame analyzed: {self.metric_analyzer.nb_frames}\n")
        report_file.write(f"Number of mostly tracked objects: {self.metric_analyzer.nb_mostly_tracked_obj}\n")
        report_file.write(f"Number of mostly lost objects: {self.metric_analyzer.nb_mostly_lost_obj}\n")
        report_file.write(f"Number of fragmentations: {self.metric_analyzer.nb_fragmentations}\n")
        report_file.write(f"Number of object misses: {self.metric_analyzer.nb_misses}\n")
        report_file.write(f"Number of false positive detection: {self.metric_analyzer.false_positive}\n")
        report_file.write(f"Number of switch: {self.metric_analyzer.switch}\n")
        report_file.write(f"MOTA: {self.metric_analyzer.mota}\n")
        report_file.write(f"MOTP: {self.metric_analyzer.motp}\n")
        report_file.write(f"Total number of inclusions: {self.metric_analyzer.nb_inclusion}\n")
        report_file.write(f"First inclusion at frame: {self.metric_analyzer.frame_index_first_inclusion}\n")
        # 2- KPI
        report_file.write(f"{self.TEXT_DELIMITER}\n")
        report_file.write("### KPI ###\n")

    def _set_report_filename(self, output_path, is_xlsx):
        """
        Set the filename of the report file

        Args:
            output_path: path where the report has to be created

        Returns: the path of the report file

        """
        suffix = '.xlsx' if is_xlsx else '.txt'
        report_name = f"{self.REPORT_PREFIX}_{self.lt_version}{suffix}"
        return os.path.join(output_path, report_name)
