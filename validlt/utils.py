# -*- coding: utf-8 -*-
"""
utils - contains the util functions used by the project

@author: Thomas Bardot
"""
import validlt.validlt_global_var as gvar
import zipfile
import os


def v_print(*args, **kwargs):
    if gvar.verbose:
        print(*args, **kwargs)
    line = ' '.join([str(a) for a in args])
    gvar.log_file.write(line + '\n')


def v_print(verbose, *args, **kwargs):
    if verbose:
        print(*args, **kwargs)
    line = ' '.join([str(a) for a in args]) + '\n'
    return line


def make_zip(files, zip_file_path):
    """
    Make a .zip archive containing all files listed in files argument

    Args:
        files(String): list of file path
        zip_file_path(String): path of the zip file to create.

    Returns:

    """
    ziph = zipfile.ZipFile(zip_file_path, 'w', zipfile.ZIP_DEFLATED)
    for file in files:
        if os.path.isfile(file):
            ziph.write(os.path.abspath(file), arcname=os.path.basename(file))
    ziph.close()


def get_string_date(date):
    """
    Args:
        date: date

    Returns: the date in string format

    """
    string_date = f"{date.year}_{date.month}_" \
                  f"{date.day}_{date.hour}" \
                  f"_{date.minute}_{date.second}"
    return string_date
