# -*- coding: utf-8 -*-
"""
@Filename : compute_kpi

Description : entry point for command line use of computekpi tool

Created on oct. 15, 2019 at 18:21:41

@author: thomas.bardot
"""
import getopt
import sys
import os.path
import re
import validlt
from validlt.computekpi.metrics_versionlt import compute_versionlt_metrics


def setup(argv):
    """
    Setup the tool from a list of arguments

    Args:
        argv: list of arguments given by the user

    Returns: the path where the capsules are stored and the list of capsule to analyze and the version of Lidar Tracker
     which has to be analyzed.
    """
    caps_path = os.path.abspath(os.getcwd())
    gen_report = False
    capsule_list = []
    version_lt = ""

    try:
        opts, args = getopt.getopt(argv, "hrp:l:", ["help", "report", "path=", "list"])
        for opt, arg in opts:
            if opt in ('-h', '--help'):
                usage()
                sys.exit(0)
            elif opt in ('-r', '--report'):
                gen_report = True
            elif opt in ("-p", "--path"):
                caps_path = arg
            elif opt in ("-l", "--list"):
                for argument in [p for p in re.split("( |\\\".*?\\\"|'.*?')", arg) if p.strip()]:
                    capsule_list.append(argument)
            else:
                msg = f"Option {opt} is not recognized."
                print(msg)
                usage()
                sys.exit(2)
        if len(args) == 1:
            version_lt = args[0]
        else:
            msg = "Wrong number of arguments."
            print(msg)
            usage()
            sys.exit(2)
    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit(2)

    try:
        caps_path = os.path.abspath(caps_path)
        assert os.path.isdir(caps_path)
    except Exception as e:
        print(f"Error occurs with arguments,  the repository {os.path.dirname(caps_path)} does not exist. \n{e}")
        usage()
        sys.exit(1)

    # If no capsule are given, look for all sub repositories of path
    if not capsule_list:
        capsule_list = [subdir for subdir in os.listdir(caps_path) if os.path.isdir(os.path.join(caps_path, subdir))]

    return caps_path, gen_report, capsule_list, version_lt


def usage():
    """
    Define the tool usage when used in command line

    """
    msg = f"""
    Compute the overall metrics and KPI for a given version of Lidar Tracker. It aggregates the metrics previously 
    computed on all the scenarios contained in the capsules passed in argument.
    Version of validlt framework : {validlt.__version__}

    Usage:
      python -m validlt.compute_kpi  [-h | --help] [-r | --report] [-p <path>| --path <path>] 
                                                      [-l <capsule_list>| --list <capsule_list>] <version_lt> 

    Options:
      -h --help                   show help
      -r --report                 generate report
      -p --path                   specify the path where the 'capsules' - i.e. where the result of lidar tracker validation - are stored.
                                  If not specified, the current repository is used by default.
      -l --list                   List of capsule name for which the metrics have to be aggregated.Has to be quoted and separated with a space.
                                  Ex.: '"'capsule_sc_3L_ego19ms_1Car_1Truck_1Motorbike capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbike S36_2019'.
                                  If not specified, all the directories located in path are taken in account.

    Arguments:
      <version_lt>                Version of the Lidar Tracker for which the kpi have to be computed
      <capsule_list>              List of capsule name for which the metrics have to be aggregated.Has to be quoted and separated with a space.
                                  Ex.: '"'capsule_sc_3L_ego19ms_1Car_1Truck_1Motorbike capsule_sc_3L_ego25ms_2Car_1Truck_1Motorbike S36_2019'
    """
    print(msg)


if __name__ == '__main__':
    path, gen_report, capsule_list, version_lt = setup(sys.argv[1:])
    compute_versionlt_metrics(path, capsule_list, version_lt, b_report=gen_report)